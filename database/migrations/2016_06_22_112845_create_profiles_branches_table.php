<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesBranchesTable extends Migration
{
    public function up()
    {
        Schema::create('profiles_branches', function(Blueprint $table)
		{
            $table->integer('profile_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->primary(['profile_id','branch_id']);
		});
    }


    public function down()
    {
        Schema::drop('profiles_branches');
    }
}
