<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->enum('role',['admin','agent'])->default('agent')->after('remember_token');
            $table->boolean('get_newsletter')->default(0)->after('role');
            $table->boolean('is_approved')->default(0)->after('role');
        });
    }


    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->dropColumn('role');
           $table->dropColumn('get_newsletter');
           $table->dropColumn('is_approved');
        });
    }
}
