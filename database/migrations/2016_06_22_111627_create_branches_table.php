<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    public function up()
    {
        Schema::create('branches', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('company_id')->index('company_id_index');
			$table->string('name', 96);
			$table->string('registration_number', 20)->nullable();
			$table->string('registration_type', 20)->nullable();
			$table->date('date_of_registration')->nullable();
			$table->string('url')->nullable();
			$table->string('email', 96)->nullable();
			$table->string('address1', 128)->nullable();
			$table->string('address2', 128)->nullable();
			$table->string('pobox', 20)->nullable();
			$table->string('zip', 20)->nullable();
			$table->string('city', 96)->nullable();
			$table->float('longitude', 10, 0)->nullable();
			$table->float('latitude', 10, 0)->nullable();
			$table->string('phone', 32)->nullable();
			$table->string('fax', 32)->nullable();
			$table->timestamps();
		});
    }


    public function down()
    {
        Schema::drop('branches');
    }
}
