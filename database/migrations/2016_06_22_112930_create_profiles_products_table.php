<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesProductsTable extends Migration
{
    public function up()
    {
        Schema::create('profiles_products', function(Blueprint $table)
		{
			$table->integer('profile_id')->unsigned();
  		    $table->integer('product_id')->unsigned();
  			$table->boolean('tied');
			$table->primary(['profile_id','product_id']);
		});
    }


    public function down()
    {
        Schema::drop('profiles_products');
    }
}
