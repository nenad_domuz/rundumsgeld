<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnProfilesProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles_products', function (Blueprint $table) {
            $table->integer('insurance_company_id')->unsigned()->change();
//            $table->foreign('insurance_company_id')->references('id')->on('insurance_companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles_products', function (Blueprint $table) {
//            $table->dropForeign('insurance_company_id');
            $table->string('insurance_company_id')->change();
        });
    }
}
