<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrimaryKeyProfilesProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles_products', function (Blueprint $table) {
            DB::unprepared('ALTER TABLE `profiles_products` DROP PRIMARY KEY, ADD PRIMARY KEY (`profile_id`, `product_id`, `insurance_company_id`)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles_products', function (Blueprint $table) {
            //
        });
    }
}
