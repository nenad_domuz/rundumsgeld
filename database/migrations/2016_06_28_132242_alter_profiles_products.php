<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProfilesProducts extends Migration
{
    public function up()
    {
        Schema::table('profiles_products', function (Blueprint $table) {
            $table->string('company');
        });
    }


    public function down()
    {

    }
}
