<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompaniesProductsTable extends Migration
{
    public function up()
    {
        Schema::table('companies_products', function (Blueprint $table)
        {
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('company_id')->references('id')->on('companies');
	    });
    }


    public function down()
    {
        Schema::table('companies_products', function (Blueprint $table) {
            $table->dropForeign(['product_id']);
            $table->dropForeign(['company_id']);
        });
    }
}
