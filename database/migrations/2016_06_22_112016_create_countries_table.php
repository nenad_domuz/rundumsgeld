<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    public function up()
    {
        Schema::create('countries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 128);
			$table->string('iso_code_2', 2);
			$table->string('iso_code_3', 3);
			$table->text('address_format', 65535);
            $table->timestamps();
		});
    }


    public function down()
    {
        Schema::drop('countries');
    }
}
