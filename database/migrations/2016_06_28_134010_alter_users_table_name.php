<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableName extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->dropColumn('name');
            $table->string('firstname')->after('id');
            $table->string('lastname')->after('firstname');
        });
    }


    public function down()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->string('name')->after('id');
            $table->dropColumn(['firstname']);
            $table->dropColumn(['lastname']);
        });
    }
}
