<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnProfilesProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles_products', function (Blueprint $table) {
            $table->renameColumn('company', 'insurance_company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles_products', function (Blueprint $table) {
            $table->renameColumn('insurance_company_id', 'company');
        });
    }
}
