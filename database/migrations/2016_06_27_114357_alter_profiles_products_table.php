<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProfilesProductsTable extends Migration
{
    public function up()
    {
        Schema::table('profiles_products', function (Blueprint $table)
        {
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    public function down()
    {
        Schema::table('profiles_products', function (Blueprint $table) {
            $table->dropForeign(['profile_id']);
            $table->dropForeign(['product_id']);
        });
    }
}