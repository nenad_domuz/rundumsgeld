<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesProductsTable extends Migration
{
    public function up()
    {
        Schema::create('companies_products', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('company_id')->unsigned();
   	    	$table->integer('product_id')->unsigned();
			$table->boolean('status')->default(0);
            $table->timestamps();
		});
    }


    public function down()
    {
        Schema::drop('companies_products');
    }
}
