<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    public function up()
    {
        Schema::create('companies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 96);
			$table->string('registration_number', 20)->nullable();
			$table->string('registration_type', 20)->nullable();
			$table->date('date_of_registration')->nullable();
			$table->string('url')->nullable();
			$table->string('email', 96)->nullable();
			$table->string('address_1', 128);
			$table->string('address_2', 128)->nullable();
			$table->string('city', 96);
			$table->string('pobox', 20);
			$table->string('zip', 20);
			$table->integer('country_id')->unsigned();
			$table->float('latitude', 10, 0)->nullable();
			$table->float('longitude', 10, 0)->nullable();
			$table->string('phone', 32)->nullable();
			$table->string('fax', 32)->nullable();
			$table->text('products', 65535)->nullable();
			$table->timestamps();
		});
    }


    public function down()
    {
        Schema::drop('companies');
    }
}
