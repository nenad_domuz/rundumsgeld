<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->date('date_of_birth')->nullable()->after('lastname');
            $table->string('private_address', 255)->nullable()->after('email');
            $table->string('hobbies', 255)->nullable()->after('about_me');
            $table->string('training', 255)->nullable()->after('hobbies');
            $table->string('skills', 255)->nullable()->after('training');
            $table->string('education', 255)->nullable()->after('skills');
            $table->string('language', 255)->nullable()->after('education');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn(['date_of_birth', 'private_address', 'hobbies', 'training', 'skills', 'education', 'language']);
        });
    }
}
