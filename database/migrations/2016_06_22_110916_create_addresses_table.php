<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    public function up()
    {   
        Schema::create('addresses', function(Blueprint $table)
		{
          	$table->increments('id');
            $table->integer('user_id')->unsigned();
			$table->integer('company_id');
			$table->string('address_1', 128);
			$table->string('address_2', 128);
			$table->string('city', 128);
			$table->string('zip', 10);
			$table->integer('country_id')->unsigned();
            $table->timestamps();
		});
    }


    public function down()
    {
        Schema::drop('addresses');
    }
}
