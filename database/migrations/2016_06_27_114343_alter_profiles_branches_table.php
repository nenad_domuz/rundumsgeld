<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProfilesBranchesTable extends Migration
{
    public function up()
    {
        Schema::table('profiles_branches', function (Blueprint $table)
        {
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    public function down()
    {
        Schema::table('profiles_branches', function (Blueprint $table) {
            $table->dropForeign(['profile_id']);
            $table->dropForeign(['branch_id']);
        });
    }
}