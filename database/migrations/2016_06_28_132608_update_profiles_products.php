<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Helpers\StringHelper;
use App\Helpers\DataBaseHelper;
use App\Product;

class UpdateProfilesProducts extends Migration
{
    public function up()
    {
        DB::table('profiles_products')->truncate();

        $profiles = DataBaseHelper::GetBasicProfileSearchQuery()->groupBy('profiles.id')->get();

        foreach ($profiles as $profile) {
            $profileProducts = unserialize($profile->products);
            if ($profileProducts) {
                foreach ($profileProducts as $profileProduct) {
                    $productId = DB::table('products')->select('id')->where('name', $profileProduct['name'])->first();
                    try {
                        DB::table('profiles_products')->insert(
                            [
                                'profile_id' => $profile->id,
                                'product_id' => $productId->id,
                                'tied' => ($profileProduct['status'] == 'UEbrige/Gebunden' ? 1 : 0),
                                'company' => $profileProduct['company']
                            ]
                        );
                    } catch (Exception $e)
                    {

                    }
                }
            }

        }
    }


    public function down()
    {
        Schema::table('profiles_products', function (Blueprint $table) {
            //
        });
    }
}
