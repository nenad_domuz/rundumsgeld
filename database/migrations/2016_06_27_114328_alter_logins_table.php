<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLoginsTable extends Migration
{
    public function up()
    {
        Schema::table('logins', function (Blueprint $table)
        {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::table('logins', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
    }
}