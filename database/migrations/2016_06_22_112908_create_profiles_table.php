<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    public function up()
    {
        Schema::create('profiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('finma_registration_number', 32);
			$table->string('title', 5);
			$table->string('firstname', 32);
			$table->string('lastname', 32);
			$table->string('email', 96);
			$table->string('phone', 32);
			$table->string('fax', 32);
			$table->string('mobile', 32);
			$table->string('url')->nullable();
			$table->string('position', 32)->nullable();
			$table->string('registration_type', 32)->nullable();
			$table->string('registration_number', 20)->nullable();
			$table->date('date_of_registration')->nullable();
			$table->string('image')->nullable();
			$table->integer('viewed')->default(0);
			$table->integer('rank')->default(0);
			$table->text('about_me', 65535)->nullable();
			$table->text('products', 65535);
			$table->timestamps();
		});
    }


    public function down()
    {
        Schema::drop('profiles');
    }
}
