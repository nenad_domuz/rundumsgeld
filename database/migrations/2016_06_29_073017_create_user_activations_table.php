<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActivationsTable extends Migration
{

    public function up()
    {
        Schema::create('user_activations', function (Blueprint $table) {
            $table->increments('user_id')->unsigned();
            $table->string('token')->index();
            $table->timestamp('created_at');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }


    public function down()
    {
        Schema::drop('user_activations');
    }
}
