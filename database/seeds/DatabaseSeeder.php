<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call('BranchesTableSeeder');
        $this->call('CompaniesTableSeeder');
        $this->call('ProductsTableSeeder');
        $this->call('ProfilesTableSeeder1');
        $this->call('ProfilesTableSeeder2');
        $this->call('ProfilesTableSeeder3');
        $this->call('ProfilesBranchesTableSeeder1');
        $this->call('ProfilesBranchesTableSeeder2');
        $this->call('ProfilesBranchesTableSeeder3');
        $this->call('ProfilesBranchesTableSeeder4');
        $this->call('ProfilesProductsTableSeeder1');
        $this->call('ProfilesProductsTableSeeder2');
        $this->call('ProfilesProductsTableSeeder3');
        $this->call('ProfilesProductsTableSeeder4');
        $this->call('ProfilesProductsTableSeeder5');
        $this->call('ProfilesProductsTableSeeder6');
        $this->call('InsuranceCompaniesTableSeeder');
    }
}
