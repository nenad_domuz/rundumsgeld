<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
//         DB::table('users')->insert([
//            'firstname' => 'Thomas',
//            'lastname' => 'Panzeri',
//            'email' => 't.panzeri@asvm.ch',
//            'password' => bcrypt('rundumsgeld'),
//            'role' => 'admin',
//             'is_approved' => 1,
//             'created_at' => 'NOW()'
//        ]);

         DB::table('users')->insert([
            'firstname' => 'Nenad',
            'lastname' => 'Domuz',
            'email' => 'n.domuz@avenya.rs',
            'password' => bcrypt('rundumsgeld'),
            'role' => 'admin',
             'is_approved' => 1,
             'created_at' => 'NOW()'
        ]);

         DB::table('users')->insert([
            'firstname' => 'Aleksandar',
            'lastname' => 'Brcic',
            'email' => 'a.brcic@avenya.rs',
            'password' => bcrypt('rundumsgeld'),
            'role' => 'admin',
             'is_approved' => 1,
             'created_at' => 'NOW()'
        ]);
    }
}
