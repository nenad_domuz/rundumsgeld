<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Kollektivlebensversicherung',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Übrige Lebensversicherung',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Personenversicherung',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Kaskoversicherung',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Transportgüterversicherung',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Übrige Sachschadenversicherung',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Finanzielle Absicherungen',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Haftpflichtversicherung',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Rechtschutz',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Touristische Beistandsleistungen',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Rückversicherungen',
            ),
        ));
        
        
    }
}
