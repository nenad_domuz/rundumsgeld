<?php

use Illuminate\Database\Seeder;

class InsuranceCompaniesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('insurance_companies')->delete();
        
        \DB::table('insurance_companies')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ungebunden',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Generali Personenversicherungen',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Generali Allgemeine Versicherungen',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Fortuna Rechtsschutz',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Allianz Suisse Leben',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Helvetia Schweizerische Lebensversicherungsgesellschaft AG',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Pax Schweiz. Lebensversicherungs-Gesellschaft AG',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Swiss Life',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Schweiz. National Lebensversicherungs-Gesellschaft',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'AXA Leben',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Zürich Lebensversicherungsgesellschaft',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'AIG Life Insurance Company',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Aspecta Assurance Liechtenstein',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Fortuna Leben Liechtenstein',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Schweiz. Mobiliar Lebensversicherungs-Gesellschaft',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Skandia Leben AG',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Vaudoise Leben, Versicherungs-Gesellschaft',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Aerosana Versicherungen AG',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Allianz Suisse',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Concordia',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'CSS Versicherung AG',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Helsana Zusatzversicherungen AG',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Helvetia Schweizerische Versicherungsgesellschaft AG',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Innova Versicherungen AG',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Kolping',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Sympany',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'ÖKK-Versicherungen AG',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Sanitas Privatversicherungen AG',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Schweizerische Mobiliar Versicherungsgesellschaft AG',
            ),
            29 => 
            array (
                'id' => 30,
                'name' => 'Swica Krankenversicherung',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Vaudoise Allgemeine, Versicherungs-Gesellschaft',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Visana Versicherungen AG',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'AXA Versicherungen',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Zürich Versicherungs-Gesellschaft',
            ),
            34 => 
            array (
                'id' => 35,
                'name' => 'Epona Allgemeine Tierversicherungsgesellschaft',
            ),
            35 => 
            array (
                'id' => 36,
                'name' => 'CAP Rechtsschutz-Versicherungsgesellschaft',
            ),
            36 => 
            array (
                'id' => 37,
                'name' => 'Orion Rechtsschutz-Versicherungsgesellschaft',
            ),
            37 => 
            array (
                'id' => 38,
                'name' => 'Protekta Rechtsschutz-Versicherungs AG',
            ),
            38 => 
            array (
                'id' => 39,
                'name' => 'AXA-ARAG Rechtsschutz',
            ),
            39 => 
            array (
                'id' => 40,
                'name' => 'Mondial Assistance',
            ),
            40 => 
            array (
                'id' => 41,
                'name' => 'Europäische Reiseversicherungs AG',
            ),
            41 => 
            array (
                'id' => 42,
                'name' => 'Groupe Mutuel Assurances',
            ),
            42 => 
            array (
                'id' => 43,
                'name' => 'Euler Hermes',
            ),
            43 => 
            array (
                'id' => 44,
                'name' => 'Helsana Unfall AG',
            ),
            44 => 
            array (
                'id' => 45,
                'name' => 'DAS Rechtschutz-Versicherungs-AG',
            ),
            45 => 
            array (
                'id' => 46,
                'name' => 'Emmentalische Mobiliar-Versicherungs-Gesellschaft',
            ),
            46 => 
            array (
                'id' => 47,
                'name' => 'Schweizerische National Versicherungs-Gesellschaft',
            ),
            47 => 
            array (
                'id' => 48,
                'name' => 'Wincare Zusatzversicherungen',
            ),
            48 => 
            array (
                'id' => 49,
                'name' => 'Axa Art Versicherung AG',
            ),
            49 => 
            array (
                'id' => 50,
                'name' => 'Basler Leben AG',
            ),
            50 => 
            array (
                'id' => 51,
                'name' => 'Assura S.A.',
            ),
            51 => 
            array (
                'id' => 52,
                'name' => 'Basler Versicherung AG',
            ),
            52 => 
            array (
                'id' => 53,
                'name' => 'Intras Assurances',
            ),
            53 => 
            array (
                'id' => 54,
                'name' => 'Swica Versicherungen',
            ),
            54 => 
            array (
                'id' => 55,
                'name' => 'Schweiz. National Versicherungs-Gesellschaft',
            ),
            55 => 
            array (
                'id' => 56,
                'name' => 'Coop Rechtsschutz',
            ),
            56 => 
            array (
                'id' => 57,
                'name' => 'Solida Versicherungen AG',
            ),
            57 => 
            array (
                'id' => 58,
                'name' => 'Assista TCS SA',
            ),
            58 => 
            array (
                'id' => 59,
                'name' => 'Chubb Insurance',
            ),
            59 => 
            array (
                'id' => 60,
                'name' => 'TSM Transportversicherungsgesellschaft',
            ),
            60 => 
            array (
                'id' => 61,
                'name' => 'Agrisano',
            ),
            61 => 
            array (
                'id' => 62,
                'name' => 'TCS Assurances SA',
            ),
            62 => 
            array (
                'id' => 63,
                'name' => 'Valorlife',
            ),
            63 => 
            array (
                'id' => 64,
                'name' => 'Europ Assistance',
            ),
            64 => 
            array (
                'id' => 65,
                'name' => 'Atupri',
            ),
            65 => 
            array (
                'id' => 66,
                'name' => 'Avenir',
            ),
            66 => 
            array (
                'id' => 67,
                'name' => 'Easy Sana',
            ),
            67 => 
            array (
                'id' => 68,
                'name' => 'Philos',
            ),
            68 => 
            array (
                'id' => 69,
                'name' => 'Phenix Lebensversicherungsgesellschaft',
            ),
            69 => 
            array (
                'id' => 70,
                'name' => 'Phenix Versicherungsgesellschaft',
            ),
            70 => 
            array (
                'id' => 71,
                'name' => 'Genfer Lebensversicherungs-Gesellschaft',
            ),
            71 => 
            array (
                'id' => 72,
                'name' => 'Genfer Allgemeine Versicherungs-Gesellschaft',
            ),
            72 => 
            array (
                'id' => 73,
                'name' => 'Groupe Mutuel Vie',
            ),
            73 => 
            array (
                'id' => 74,
                'name' => 'Liechtenstein Life Assurance AG',
            ),
            74 => 
            array (
                'id' => 75,
                'name' => 'Zenith Leben, Lebensversicherungsgesellschaft',
            ),
            75 => 
            array (
                'id' => 76,
                'name' => 'AIG Europe Limited',
            ),
            76 => 
            array (
                'id' => 77,
                'name' => 'Mannheimer Versicherung AG',
            ),
            77 => 
            array (
                'id' => 78,
                'name' => 'Dextra Rechtsschutz AG',
            ),
            78 => 
            array (
                'id' => 79,
                'name' => 'UNIQA Lebensversicherung AG',
            ),
            79 => 
            array (
                'id' => 80,
                'name' => 'Coop Allgemeine Versicherung AG',
            ),
            80 => 
            array (
                'id' => 81,
                'name' => 'Helsana Rechtsschutz',
            ),
            81 => 
            array (
                'id' => 82,
                'name' => 'Hermes KK',
            ),
            82 => 
            array (
                'id' => 83,
                'name' => 'Cigna',
            ),
            83 => 
            array (
                'id' => 84,
                'name' => 'Lloyd`s',
            ),
            84 => 
            array (
                'id' => 85,
                'name' => 'Allianz Risk Transfer',
            ),
            85 => 
            array (
                'id' => 86,
                'name' => 'Alba Allgemeine Versicherungs-Gesellschaft',
            ),
            86 => 
            array (
                'id' => 87,
                'name' => 'Mutualité Assurances',
            ),
            87 => 
            array (
                'id' => 88,
                'name' => 'GAN Risques divers',
            ),
            88 => 
            array (
                'id' => 89,
                'name' => 'La Genevoise, Compagnie générale d\'Assurances',
            ),
            89 => 
            array (
                'id' => 90,
                'name' => 'La Suisse Versicherungsgesellschaft',
            ),
            90 => 
            array (
                'id' => 91,
                'name' => 'Schweiz Allgemeine',
            ),
            91 => 
            array (
                'id' => 92,
                'name' => 'Forces vives',
            ),
            92 => 
            array (
                'id' => 93,
                'name' => 'UNIQA Versicherung AG',
            ),
            93 => 
            array (
                'id' => 94,
                'name' => 'Credit Suisse Life & Pensions',
            ),
            94 => 
            array (
                'id' => 95,
                'name' => 'Metzger Versicherungen',
            ),
            95 => 
            array (
                'id' => 96,
                'name' => 'ACE Insurance',
            ),
            96 => 
            array (
                'id' => 97,
                'name' => 'Hotela',
            ),
            97 => 
            array (
                'id' => 98,
                'name' => 'Convia Lebensversicherungs-Gesellschaft',
            ),
            98 => 
            array (
                'id' => 99,
                'name' => 'Eidg. Gesundheitskasse',
            ),
            99 => 
            array (
                'id' => 100,
                'name' => 'HDI-Gerling Industrie Versicherung AG',
            ),
            100 => 
            array (
                'id' => 101,
                'name' => 'S.O.S. Evasan Compagnie d\'assurance-assistance',
            ),
            101 => 
            array (
                'id' => 102,
                'name' => 'VVST - Versicherungs-Verband Schweiz. Transportunt',
            ),
            102 => 
            array (
                'id' => 103,
                'name' => 'CMBB',
            ),
            103 => 
            array (
                'id' => 104,
                'name' => 'Swiss Re',
            ),
            104 => 
            array (
                'id' => 105,
                'name' => 'Auxilia',
            ),
            105 => 
            array (
                'id' => 106,
                'name' => 'Appenzellische Feuer',
            ),
            106 => 
            array (
                'id' => 107,
                'name' => 'Mutuelle Valaisanne',
            ),
            107 => 
            array (
                'id' => 108,
                'name' => 'GE Financial Insurance',
            ),
            108 => 
            array (
                'id' => 109,
                'name' => 'Império',
            ),
            109 => 
            array (
                'id' => 110,
                'name' => 'Liberty Mutual',
            ),
            110 => 
            array (
                'id' => 111,
                'name' => 'Supra Assurances',
            ),
            111 => 
            array (
                'id' => 112,
                'name' => 'Aspen Insurance UK Limited',
            ),
            112 => 
            array (
                'id' => 113,
                'name' => 'KPT Versicherungen',
            ),
            113 => 
            array (
                'id' => 114,
            'name' => 'UNIQA (ex Austria)',
            ),
            114 => 
            array (
                'id' => 115,
                'name' => 'Universa',
            ),
            115 => 
            array (
                'id' => 116,
                'name' => 'Xundheit, Öffentliche Gesundheitsk.',
            ),
            116 => 
            array (
                'id' => 117,
                'name' => 'XL Europe',
            ),
            117 => 
            array (
                'id' => 118,
            'name' => 'QBE Insurance (Europe) Ltd.',
            ),
            118 => 
            array (
                'id' => 119,
                'name' => 'CNA Insurance Company Ltd.',
            ),
            119 => 
            array (
                'id' => 120,
                'name' => 'Krankenkasse Steffisburg',
            ),
            120 => 
            array (
                'id' => 121,
                'name' => 'Lombard Int.',
            ),
            121 => 
            array (
                'id' => 122,
                'name' => 'Euler Hermes UK',
            ),
            122 => 
            array (
                'id' => 123,
            'name' => 'Avantis (= Ex Orsières)',
            ),
            123 => 
            array (
                'id' => 124,
                'name' => 'Caisse Vaudoise',
            ),
            124 => 
            array (
                'id' => 125,
                'name' => 'Fonction Publique',
            ),
            125 => 
            array (
                'id' => 126,
                'name' => 'Futura',
            ),
            126 => 
            array (
                'id' => 127,
                'name' => 'Natura caisse de santé',
            ),
            127 => 
            array (
                'id' => 128,
                'name' => 'St. Moritz',
            ),
            128 => 
            array (
                'id' => 129,
                'name' => 'Troistorrents',
            ),
            129 => 
            array (
                'id' => 130,
                'name' => 'Inter Partner',
            ),
            130 => 
            array (
                'id' => 131,
                'name' => 'London General Insurance',
            ),
            131 => 
            array (
                'id' => 132,
                'name' => 'Heerbrugg',
            ),
            132 => 
            array (
                'id' => 133,
            'name' => 'Atradius Kreditversicherung (ex Gerling NCM)',
            ),
            133 => 
            array (
                'id' => 134,
                'name' => 'Aerzteversicherung',
            ),
            134 => 
            array (
                'id' => 135,
                'name' => 'XL Versicherungen Schweiz',
            ),
            135 => 
            array (
                'id' => 136,
                'name' => 'EOS',
            ),
            136 => 
            array (
                'id' => 137,
                'name' => 'UBS Life',
            ),
        ));
        
        
    }
}
