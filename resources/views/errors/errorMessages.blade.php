@foreach ( $errors->all() as $error )
    <div class='flash alert-warning'>
        <ul class="panel-body">
            <li>
                {{ $error }}
            </li>
        </ul>
    </div>
@endforeach
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@elseif(session('warning'))
    <div class="alert alert-warning">
        {{ session('warning') }}
    </div>
@elseif(session('danger'))
    <div class="alert alert-danger">
        {{ session('danger') }}
    </div>
@endif
<script type="application/javascript">
    window.setTimeout(function() {
        $(".alert, .flash").fadeTo(300, 0).slideUp(300, function(){
            $(this).remove();
        });
    }, 5000);
</script>