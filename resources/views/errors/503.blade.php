<!DOCTYPE html>
<html>
<head>
    @include('includes.errorhead')
</head>
<body>
<div class="container-fluid">
    <header>
        <div class="image col-xs-12 col-md-12 col-lg-12">
            <a href="/"><img src="{!! asset('images/logo-header.png') !!}" alt="Rundumsgeld" title="Rundumsgeld" /></a>
        </div>
    </header>
    <div class="content">
        <div class="title">
            <p>@lang('errors.title.appDown')</p>
        </div>
    </div>
</div>
</body>
</html>
