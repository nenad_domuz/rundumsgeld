<!DOCTYPE html>
<html>
<head>
    @include('includes.errorhead')
</head>
<body>
<div class="container-fluid">
    <header>
        <div class="image col-xs-12 col-md-12 col-lg-12">
            <a href="/"><img src="{!! asset('images/logo-header.png') !!}" alt="Rundumsgeld" title="Rundumsgeld" /></a>
        </div>
    </header>
    <div class="content">
        <div class="title">
            <p>@lang('errors.title.paragraph1').</p>
            <p>@lang('errors.title.paragraph2') <a href="/">@lang('errors.title.paragraph.home')</a> @lang('errors.title.paragraph2.2')</p>
            <p>@lang('errors.title.paragraph3'):</p>
            <div class="col-md-6">
                <nav class="main">
                    <ul>
                        @if (Auth::guest())
                            <li>
                                <a href="/versicherungsberatung">@lang('errors.menu.1')</a>
                            </li>
                            <li>
                                <a href="/finanzplanung">@lang('errors.menu.2')</a>
                            </li>
                            <li>
                                <a href="/kreditberatung">@lang('errors.menu.3')</a>
                            </li>
                            <li>
                                <a href="/blog">@lang('errors.menu.4')</a>
                            </li>
                        @else
                            <li>
                                <a href="/user">@lang('errors.menu.5')</a>
                            </li>
                            <li>
                                <a href="/blog">@lang('errors.menu.4')</a>
                            </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
</body>
</html>
