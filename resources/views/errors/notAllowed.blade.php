@extends('layouts.default')

@section('title')
    Registrierung fehlgeschlagen
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <img src="{{ asset('/favicon.png') }}" alt=""/>
                    </div>
                    <div class="panel-body">

                        @include('errors.errorMessages')

                        <p>@lang('gui.register.notAllowed.text')</p><a class="btn btn-primary" href="/kontakt">@lang('gui.register.notAllowed.button')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
