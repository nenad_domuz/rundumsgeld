<!DOCTYPE html>
<html>
    <head>
        @include('includes.geolocation')
        @include('includes.head')
    </head>
    <body @if(in_array($viewName,['pages.home'], null)){!! "class='home'" !!} @endif >
    <div class="content" @if(in_array($viewName,['auth.login', 'auth.register'], null)){!! "id='page-top-auth'" !!} @else  {!! "id='page-top'" !!} @endif >
        <header>
            @include('includes.header')
        </header>
        <main>
            <div class="container">
                @if (!in_array($viewName,['pages.home','pages.profile'], null))
                    <div class="row title margin-custom">
                        <h1>@yield('title')</h1>
                    </div>
                @endif

                @yield('content')
            </div>
        </main>
    </div>

    @if(!in_array($viewName, ['auth.register', 'auth.login', 'auth.passwords.reset', 'auth.passwords.email'], null))
        <footer>
            @include('includes.footerTop')
            @include('includes.footerBottom')
        </footer>
    @endif
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57ad8216075f8b19"></script>
    </body>
</html>
