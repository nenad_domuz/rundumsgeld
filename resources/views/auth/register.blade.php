@extends('layouts.default')

@section('title')
    Registrieren
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <img src="{{ asset('/favicon.png') }}" alt=""/>
                </div>
                <div class="panel-body">
                    @include('errors.errorMessages')
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}" novalidate>
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                            <label for="firstname" class="col-md-4 control-label">@lang('gui.register.firstname')</label>
                            <div class="col-md-6">
                                <input required id="firstname" type="text" class="form-control form-required" name="firstname" value="{{ old('firstname') }}" placeholder="@lang('gui.register.firstname.placeholder')" autocomplete="off" >
                                @if ($errors->has('firstname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="col-md-4 control-label">@lang('gui.register.lastname')</label>
                            <div class="col-md-6">
                                <input required id="lastname" type="text" class="form-control form-required" name="lastname" value="{{ old('lastname') }}" placeholder="@lang('gui.register.lastname.placeholder')" autocomplete="off" >
                                @if ($errors->has('lastname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang('gui.contact.email')</label>
                            <div class="col-md-6">
                                <input required id="email" type="email" class="form-control form-required" name="email" value="{{ old('email') }}" placeholder="@lang('gui.contact.email.placeholder')" autocomplete="off" title="Please use your business email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('finma_registration_number') ? ' has-error' : '' }}">
                            <label for="finma_registration_number" class="col-md-4 control-label">@lang('gui.register.finmanumber')</label>
                            <div class="col-md-6">
                                <input id="finma_registration_number" type="text" class="form-control" name="finma_registration_number" value="{{ old('finma_registration_number') }}" placeholder="@lang('gui.register.finma.placeholder')" autocomplete="off">
                                @if ($errors->has('finma_registration_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('finma_registration_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">@lang('gui.login.password')</label>
                            <div class="col-md-6">
                                <input required id="password" type="password" class="form-control form-required" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">@lang('gui.register.confirmPassword')</label>
                            <div class="col-md-6">
                                <input required id="password-confirm" type="password" class="form-control form-required" name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> @lang('gui.register')
                                </button>
                                <a class="btn btn-default btn-close" href="{{ url('/') }}"> @lang('general.cancel')</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
