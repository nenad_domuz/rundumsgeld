@extends('layouts.default')

@section('title')
    @lang('gui.resetPassword')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <img src="{{ asset('/favicon.png') }}" alt=""/>
                </div>
                <div class="panel-body">
                    @include('errors.errorMessages')
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang('gui.contact.email')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control form-required" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-envelope"></i> @lang('gui.sendPasswordLink')
                                </button>
                                <a href="{{ url('/') }}" class="btn btn-default">@lang('general.cancel')</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
