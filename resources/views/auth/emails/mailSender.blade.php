@extends('auth.emails.templates.htmlTemplate')

@section('title')
    Subscribe
@endsection

@section('text')
    <a href="{!! "http://localhost:8000/unsubscribe/$token/" . $profile_id !!}">Unsubscribe</a>
@endsection