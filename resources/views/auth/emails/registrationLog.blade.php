@extends('auth.emails.templates.htmlTemplate')

@section('title')
    Registration Log
@endsection

@section('text')
    <p>User: {{ $firstname }} {{ $lastname }} </p>
    <p>Email: {{ $email }}</p>
@endsection
