<!DOCTYPE html>
<html>
<head>
    <title>Rund ums Geld</title>
</head>
<body>
<div class="container">
    <div class="content">
        <h2>@yield('title'):</h2>
        @yield('text')
    </div>
</div>
</body>
</html>