@extends('auth.emails.templates.htmlTemplate')

@section('title')
    @lang('emails.password.title')
@endsection

@section('text')
    {!! trans('emails.password.text', ['link' => url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset())]) !!}
@endsection