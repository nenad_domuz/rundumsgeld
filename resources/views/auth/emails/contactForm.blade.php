@extends('auth.emails.templates.plainTextTemplate')

@section('title')
    You've received an email from the contact form
@endsection

@section('text')
    <p>Name: <span style="color: #3f65b1">{{ ucwords($name) }}</span></p>
    <p>Email: <span style="color: #3f65b1">{{ $email }}</span></p>
    <p>Website: {{ $website }}</p>
    <p>Message: {{ $question }}</p>
@endsection
