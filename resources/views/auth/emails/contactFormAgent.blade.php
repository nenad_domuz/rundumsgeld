@extends('auth.emails.templates.htmlTemplate')

@section('title')
    @lang('emails.contact.agent.email.title')
@endsection

@section('text')
    <p>Agent name: <span style="color: #3f65b1">{{ ucwords($agentFirstname) . ' ' . ucwords($agentLastname) }}</span></p>
    <p>Agent Id: <span style="color: #3f65b1">{{ $agentId }}</span></p>
    <p>Agent Email: <span style="color: #3f65b1">{{ $agentEmail }}</span></p>
    <p>Sender Name: <span style="color: #3f65b1">{{ ucwords($senderName) }}</span></p>
    <p>Sender Email: <span style="color: #3f65b1">{{ $senderEmail }}</span></p>
    <p>Sender Message: {{ $senderMessage }}</p>
@endsection
