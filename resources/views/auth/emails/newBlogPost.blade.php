@extends('auth.emails.templates.htmlTemplate')

@section('title')
    {!! trans('emails.blog.newPost.title') !!}
@endsection

@section('text')
    <p>Post title: <span style="color: #3f65b1">{{ $title }}</span></p>
    <p>User id: <span style="color: #3f65b1">{{ $userId }}</span></p>
@endsection