@extends('auth.emails.templates.htmlTemplate')

@section('title')
    {!! trans('emails.activation.title', ['link' => url('user/activation', $token)]) !!}
@endsection

@section('text')
    {!! trans('emails.activation.text', ['firstname' => ucwords($firstname), 'lastname' => ucwords($lastname), 'finmaNumber' => $finmaNumber]) !!}
@endsection