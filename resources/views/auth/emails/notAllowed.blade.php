@extends('auth.emails.templates.plainTextTemplate')

@section('title')
    Registration Fail Log
@endsection

@section('text')
    <p>User: {{ $firstname }} {{ $lastname }} </p>
    <p>Email: {{ $email }}</p>
@endsection
