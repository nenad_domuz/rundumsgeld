@extends('auth.emails.templates.htmlTemplate')

@section('title')
    Here are your credentials, {{ $name }}
@endsection

@section('text')
    <p>Your username: {{ $user }}</p>
    <p>Your email: {{ $email }}</p>
@endsection

