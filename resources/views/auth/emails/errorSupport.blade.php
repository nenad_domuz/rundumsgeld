@extends('auth.emails.templates.plainTextTemplate')

@section('title')
    Error
@endsection

@section('text')
    <p>{{ $error }}</p>
@endsection
