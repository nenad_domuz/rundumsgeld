@extends('auth.emails.templates.plainTextTemplate')

@section('title')
    User {{ ucwords($firstname) . PHP_EOL . ucwords($lastname) }} has updated his profile
@endsection

@section('text')
    <p>Email: {{ $email }}</p>
    <p>Link: <a href="{!! "https://www.rundumsgeld.ch".$link !!}">{{ $firstname . PHP_EOL . $lastname }}</a></p>
@endsection
