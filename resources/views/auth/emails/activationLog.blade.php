@extends('auth.emails.templates.htmlTemplate')

@section('title')
    Activation Log
@endsection

@section('text')
    <p>User: {{ $firstname }} {{$lastname}} </p>
    <p>Email: {{ $email }}</p>
    <p>Link: <a href="{{ $link }}">{{ $link }}</a></p>
@endsection
