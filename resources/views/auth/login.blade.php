@extends('layouts.default')

@section('title')
    Login
@endsection

@section('content')
    <div class="container">
        <p>Melden Sie sich an, um alle Funktionen von Rund ums Geld benutzen zu können.</p>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <img src="{{ asset('/favicon.png') }}" alt=""/>
                        <a class="btn-link" href="{{ url('/password/reset') }}" style="display:inline-block;float:right;">
                            <small>@lang('gui.login.forgotPassword')</small>
                        </a>
                    </div>
                    <div class="panel-body">
                        @include('errors.errorMessages')
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" >
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-3 control-label">@lang('gui.contact.email')</label>
                                <div class="col-md-7">
                                    <input required id="email" type="email" class="form-control form-required" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-3 control-label">@lang('gui.login.password')</label>
                                <div class="col-md-7">
                                    <input required id="password" type="password" class="form-control form-required" name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3"></div>
                                <div class="col-xs-12 col-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-sign-in"></i>&nbsp;@lang('gui.login')
                                    </button>
                                    <a class="pull-right btn btn-default" href="{{ url('/') }}">@lang('general.cancel')</a>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> @lang('gui.login.rememberMe')
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
