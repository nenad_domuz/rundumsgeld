{!! $head !!}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    @foreach ($entries as $entry)
    <url>
        <loc>{{ $entry['loc'] }}</loc>
        <changefreq>{{ $entry['changefreq'] }}</changefreq>
        <lastmod>{{ $entry['lastmod'] }}</lastmod>
        <priority>{{ $entry['priority'] }}</priority>
    </url>
    @endforeach
</urlset> 