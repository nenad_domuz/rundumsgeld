@extends('layouts.default')

@section('title')
    @lang('gui.userActivated')
@endsection
@section('content')
    @include('errors.errorMessages')
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <a class="pull-right btn btn-primary" href="/user">@lang('gui.userActivated.goToProfile')</a>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    </div>
@endsection
