@extends('layouts.default')

@section('title')
Kontakt
@endsection

@section('content')
<p>Gerne  nehmen wir Anregungen, Verbesserungsvorschläge, Wünsche und Anregungen entgegen. Verwenden Sie einfach das untenstehende Formular, um uns eine Nachricht zukommen zu lassen.</p>
<div class="negative-10">
    <div class="col-xs-12 col-sm-6">
@include('includes.contactform')
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="map-holder">
            <div class="map-contact">
                <div id="gmap_canvas" style="height:100%;width:100%;"></div>
            </div>
        </div>
        <div class="contact-box">
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="col-wrap">
                    <i class="pe-7s-map"></i>
                    <h3>Adresse</h3>
                    <p>Schaffhauserstrasse 470, 8052 Zürich</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="col-wrap">
                    <i class="pe-7s-call"></i>
                    <h3>Telefon</h3>
                    <p><a href="tel:+41 43 299 60 00">+41 43 299 60 00</a></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="col-wrap">
                    <i class="pe-7s-mail-open-file"></i>
                    <h3>Email</h3>
                    <p><a href="mailto:info@rundumsgeld.ch">info@rundumsgeld.ch</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
<script type='text/javascript'>

    function initMap() {
        var myLatLng = {lat: 47.419667, lng: 8.548760};

        var map = new google.maps.Map(document.getElementById('gmap_canvas'), {
            zoom: 16,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!'
        });
    }

</script>
