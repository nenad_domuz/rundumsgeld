@extends('layouts.default')

@section('title')
@lang('gui.menu.yourProfile')<br />
<small>wilkommen {{ $userdata->firstname }}&nbsp;{{ $userdata->lastname }}</small>
@endsection

@section('content')
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
    @include('errors.errorMessages')
    <h2>Admin profile</h2>
</div>
<div class="col-xs-1 col-sm-12 col-md-4 col-lg-1"></div>
<div class="col-xs-10 col-sm-12 col-md-4 col-lg-8">
</div>
@endsection
