<style>
.cropArea {
    margin: 10px 0;
}
.cropped {
    margin: 10px 0;
}
</style>
<form role="form" method="POST" action="/">
{{ csrf_field() }}
<div ng-app="app" ng-controller="Ctrl">
    <div>
        Laden Sie ein Bild von sich hoch: <input type="file" id="fileInput" /></div>
    <div class="cropArea">
        <img-crop image="myImage" result-image="myCroppedImage" result-image-size="280" result-image-quality="1" change-on-fly="true" area-type="square"></img-crop>
    </div>
    <div ng-if="myImage" class="cropped">
        <img ng-src="@{{ myCroppedImage }}">
    </div>
    <input class="btn btn-primary" value="@lang('gui.picture.upload')" type="button"  ng-click="sendImageData()">
</div>
</form>
