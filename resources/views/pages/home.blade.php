@extends('layouts.default')

@section('title')
    @lang('gui.home.title')
@endsection

@section('content')
<div class="home-search">
    <div class="col-xs-hidden col-sm-1 col-lg-2"></div>
    <div class="col-xs-12 col-sm-10 col-lg-8 text-center">
        <h1>Finden Sie Ihren Berater</h1>
        <div class="separator"></div>
        <p class="subtitle">Rund ums Geld findet Ihren Berater in Ihrer Stadt</p>
    </div>
    <div class="col-xs-hidden col-sm-1 col-lg-2 col-lg-4"></div>
    <div class="clear"></div>
    <div class="col-xs-1 col-md-3 col-lg-4"></div>
    <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="type-box">
            <div class="type-box-wrapper">
                <form role="form" id="home-search" method="get" action="{{ url('/stadt') }}">
                    <input id="search-city" type="text" class="search-field" name="stadt" placeholder="@lang('gui.home.searchCity.placeholder')" />
                </form>
                <script type="application/javascript">
                    $('#search-city').autocomplete({
                        serviceUrl: '/search/json',
                        onSelect: function (suggestion) {
                            $(this).val(suggestion.value);
                        }
                    });
                </script>
            </div>
            <button type="submit" form="home-search"><i class="pe-7s-search"></i></button>
        </div>
    </div>
    <div class="col-xs-1 col-md-3 col-lg-4"></div>
    <div class="clear"></div>
</div>
@include('modules.boxes')
@endsection
