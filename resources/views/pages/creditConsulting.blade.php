@extends('layouts.default')

@section('title')
Kreditberatung
@endsection

@section('content')
    <p>Wenn das Leben seine Bahnen zieht, muss es manchmal schnell gehen, besonders
        wenn es ums Geld geht. Ein Kredit ermöglicht es, schon zum Zeitpunkt der
        Beantragung über Kapital zu verfügen, welches sonst erst in Zukunft bereitstehen
        würde. Mit dem neuerlangten Kapital können Sie Ihre Wünsche nach einem neuen
        Auto, neuen Möbeln, einer Ausbildung, egal was es ist, erfüllen. </p>
    <p>Die im Privat- und Konsumkredit Bereich tätigen Berater, welche auf Rund ums
        Geld aufgeführt sind, werden Sie tatkräftig bei Ihrer Entscheidung unterstützen
        und Sie mit bestem Wissen und Gewissen beraten. Die Höhe eines Kredits genau
        einzuschätzen ist nicht immer einfach, deswegen empfehlen wir Ihnen einen
        kompetenten Berater auszuwählen. Einer der zum einen Ihre momentane Lage
        einschätzt, sich zum anderen genau mit der Situation der Zinse auskennt und
        immer auf dem neuesten Stand der Dinge ist.</p>
    <h2>Die Beweggründe für einen Kredit</h2>
    <ul class="fa-ul">
        <li><i class="fa-li fa fa-check"></i>Kauf oder Leasing von Autos</li>
        <li><i class="fa-li fa fa-check"></i>Kauf von Wohnaccessoires wie Möbel und Haushaltsapparate</li>
        <li><i class="fa-li fa fa-check"></i>Investition in die Zukunft → Aus- oder Weiterbildung</li>
        <li><i class="fa-li fa fa-check"></i>Investition in ein Projekt oder in die Gründung einer eigenen Unternehmung</li>
        <li><i class="fa-li fa fa-check"></i>Investition in die Schönheit → Nasenkorrektur, Fett absaugen usw.</li>
    </ul>
    <h2>Vorteile eines Kredits</h2>
    <ul class="fa-ul">
        <li><i class="fa-li fa fa-check"></i>Sofortige Finanzierung eines Kauf- oder Investitionswunsches</li>
        <li><i class="fa-li fa fa-check"></i>Inanspruchnahme von Rabatten und Nachlässen möglich</li>
        <li><i class="fa-li fa fa-check"></i>Verbesserung der Eigenkapitalsituation</li>
        <li><i class="fa-li fa fa-check"></i>Umschuldung möglich</li>
        <li><i class="fa-li fa fa-check"></i>Finanzierbarkeit größerer Kaufvorhaben gegeben</li>
        <li><i class="fa-li fa fa-check"></i>Finanzierung auf Zeit mit geringer monatlicher Belastung möglich</li>
    </ul>
    <h2>Nachteile eines Kredits:</h2>
    <ul class="fa-ul">
        <li><i class="fa-li fa fa-check"></i>Gefahr der Überschuldung, wenn kein Berater zur Seite steht</li>
        <li><i class="fa-li fa fa-check"></i>Bindung an Finanzpartner</li>
    </ul>
    <h2>In wenigen Schritten zum geeigneten Berater gelangen</h2>
    <ul class="fa-ul">
        <li>
            <i class="fa-li fa fa-check"></i>
            Den ersten Schritt haben Sie schon gemacht, indem Sie die Seite Rund ums
            Geld ausgewählt haben.
        </li>
        <li>
            <i class="fa-li fa fa-check"></i>
            Klicken Sie unten auf Berater suchen und wählen Sie einen aus. Es
            empfiehlt sich meistens einer, der in Ihrer Nähe wohnt.
        </li>
        <li>
            <i class="fa-li fa fa-check"></i>
            Gehen Sie auf das Profil des Beraters und klicken Sie auf Book now.
            Hinterlassen Sie ihre Kontaktdaten und der Berater wird sich in kürzester
            Zeit bei Ihnen melden.
        </li>
    </ul>
@include('modules.userActionButton')
@endsection