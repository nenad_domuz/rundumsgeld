@extends('layouts.default')

@section('title')
    @lang('gui.search.titlePrefix') {{ \App\Helpers\StringHelper::getTitleForSearchResult(Request::fullurl()) }}
@endsection

@section('content')
<div class="negative-10">
    <div class="col-xs-12 col-md-6">
        <div class="negative-10 search-results" itemscope itemtype ="http://schema.org/Person">
            @foreach ($Profiles as $Profile)
            <div class="col-xs-6 col-md-6 consultant-box">
                <a href="{{ \App\Helpers\StringHelper::createProfilePageUrl($Profile->CompanyName, $Profile->lastname, $Profile->firstname, $Profile->id) }}" class="link-box">
                    <span class="image" style="">
                        <img src="{{ empty($Profile->image_blob) ? "/images/no_image2.png" : $Profile->image_blob  }}" alt="{{ $Profile->name }}" height="291">
                    </span>
                    <span itemprop="givenName" class="title">
                        {{ $Profile->firstname }} {{ $Profile->lastname }}
                    </span>
                    <span class="city"></span>
                </a>
                <div class="additional">
                   <span class="distance" id="distance{{ $Profile->id }}">@lang('gui.search.distance'): @lang('helper.string.notAvailable')</span>
                    <div class="showLocation" onmouseover="hover({{ $Profile->id }})" onmouseout="out({{ $Profile->id }})">@lang('gui.search.viewOnMap')&nbsp;&nbsp;<i class="fa fa-map-marker"></i></div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            @if (isset($_COOKIE['lat']))
            <script>
                var lat = {{$_COOKIE['lat']}};
                var lng = {{$_COOKIE['lng']}};
                var id = {{ $Profile->id}};
                    $.ajax({
                         type: "GET",
                         url: '/distance',
                         data: {lat:lat, lng:lng, id:id},
                         success: function( msg) {
                             $('#distance' + {{$Profile->id}}).html('@lang('gui.search.distance'): ' + msg);
                         }
                    });
            </script>
            @endif
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        @include('includes.searchResultMap')
    </div>
    <div class="clear"></div>
    <div class="paging">
        {{ $Profiles->links() }}
    </div>
</div>
@endsection