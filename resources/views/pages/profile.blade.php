@extends('layouts.default')

@section('content')
    <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <ddiv id="error"></ddiv>
                <form class="form-horizontal" role="form" method="POST" id="agentContactForm" data-toggle="validator">
                    <input type="hidden" id="agentFirstname" name="agentFirstname" value="{{ $profile->firstname }}">
                    <input type="hidden" id="agentLastname" name="agentLastname" value="{{ $profile->lastname }}">
                    <input type="hidden" id="agentId" name="agentId" value="{{ $profile->id }}">
                    <input type="hidden" id="agentEmail" name="agentEmail" value="{{ $profile->email }}">
                    {{ csrf_field() }}
                    <div class="modal-header agent-contact-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title" id="contactModalLabel">
                            <img src="{{ asset('/favicon.png') }}" alt="Rundumsgeld" />
                            @lang('gui.modal.title')
                        </h5>
                    </div>
                    <div class="modal-body agent-contact-body">
                        <p>@lang('gui.modal.header')</p>
                            <div class="form-group">
                                <label for="senderName" class="col-sm-3 control-label">@lang('gui.contact.name')</label>
                                <div class="col-sm-9">
                                    <input type="text" name="senderName" class="form-control form-required" id="senderName" placeholder="@lang('gui.contact.name.placeholder')" autocomplete="off">
                                    <span class="help-block">
                                        <strong style="color:#a94442" id="nameError"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="senderEmail" class="col-sm-3 control-label">@lang('gui.contact.email')</label>
                                <div class="col-sm-9">
                                    <input type="text" name="senderEmail" class="form-control form-required" id="senderEmail" placeholder="@lang('gui.contact.email.placeholder')" autocomplete="off">
                                    <span class="help-block">
                                        <strong style="color:#a94442" id="mailError"></strong>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="senderMessage" class="col-sm-3 control-label">@lang('gui.contact.message')</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control form-required" name="senderMessage" id="senderMessage" rows="5" autocomplete="off"></textarea>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <button class="btn btn-primary" style="margin-bottom: 0" type="submit" id="send">
                                <i class="fa fa-send" aria-hidden="true"></i>
                                @lang('gui.modal.buttonSend')
                            </button>
                            <a href="#" class="btn btn-default pull-right" data-dismiss="modal">@lang('gui.modal.buttonClose')</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
            $.ajaxSetup({
                headers: { 'X-CSRF-Token': $('input[name="_token"]').val() }
            });

            $("#agentContactForm").submit(function(e) {
                e.preventDefault();

                var senderName = $('#senderName').val(),
                    senderEmail = $('#senderEmail').val(),
                    senderMessage = $('#senderMessage').val(),
                    agentFirstname = $('#agentFirstname').val(),
                    agentLastname = $('#agentLastname').val(),
                    agentEmail = $('#agentEmail').val(),
                    agentId = $('#agentId').val();

                $.ajax({
                    url: "/agent",
                    type: 'POST',
                    data: {
                        senderName: senderName,
                        senderEmail: senderEmail,
                        senderMessage: senderMessage,
                        agentFirstname: agentFirstname,
                        agentLastname: agentLastname,
                        agentEmail: agentEmail,
                        agentId: agentId
                    },
                    success: function () {
                        $('#contactModal').modal('toggle');
                        console.log('Success');
                    },
                    error: function (data) {
                        var jsonObject = JSON.parse(data.responseText);
                        console.log(jsonObject);

                        $('#mailError').text(jsonObject.senderEmail[0]);
                        $('#nameError').text(jsonObject.senderName[0])
                    }
                });

                $('.modal').on('hidden.bs.modal', function(){
                    $(this).find('form')[0].reset();
                    $('#mailError, #nameError').text('');
                });
            });
    </script>
    <div class="row" itemscope itemtype ="http://schema.org/Person">
        <div class="col-sm-9 profile-custom">
            <div class="title row margin-custom" itemprop="givenName">
                <h1>{{ $profile->firstname }} {{ $profile->lastname }}</h1>
                {{\App\Helpers\DataBaseHelper::ViewCounterProfile($profile->id, $_SERVER['REMOTE_ADDR'])}}
            </div>
            <div class="pull-left image-accent image-border col-xs-12 col-sm-6 col-md-4 avatar-custom">
                <img itemprop="image" src="{{ empty($profile->image_blob) ?  '/images/no_image2.png' : $profile->image_blob }}" />
            </div>
            <div class="col-md-6">
                @if (!empty($profile->about_me))
                    <table class="borderless-custom" border="0">
                        <tbody>
                        <tr>
                            <td colspan="2" class="title-custom">@lang('gui.profile.aboutMe')</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="title-custom"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">{{ wordwrap($profile->about_me, 75, PHP_EOL, true) }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="title-custom"></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                @else
                    <p>@lang('gui.profile.aboutMe.noInfo')</p>
                @endif

                <button type="button" data-toggle="modal" data-target="#contactModal" class="btn btn-accent">@lang('gui.modal.buttonContact')</button>
            </div>

            @if(!empty($profile->skills) || !empty($profile->education) || !empty($profile->hobbies))
                <div class="separator"></div>
            @endif
            @if (!empty($profile->skills))
                <div class="col-lg-4" style="margin-bottom: 20px;">
                    <table class="borderless-custom" border="0">
                        <tbody>
                        <tr>
                            <td colspan="2" class="title-custom">@lang('gui.profile.skills')</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="title-custom"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">{{ $profile->skills }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="title-custom"></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            @endif
            @if (!empty($profile->education))
                <div class="col-lg-4 main-info">
                    <table class="borderless-custom" border="0">
                        <tbody>
                        <tr>
                            <td colspan="2" class="title-custom">@lang('gui.profile.education')</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="title-custom"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">{{ $profile->education }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="title-custom"></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            @endif
            @if (!empty($profile->hobbies))
                <div class="col-lg-4 main-info">
                    <table class="borderless-custom" border="0">
                        <tbody>
                        <tr>
                            <td colspan="2" class="title-custom">@lang('gui.profile.hobbies')</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="title-custom"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">{{ $profile->hobbies }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="title-custom"></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            @endif

            <div class="separator"></div>
            {{--<span class="time-to-book">Click on any of the times you prefer to book <i class="pe-7s-info"></i></span>--}}

            @include('includes.profile.additionalInformation')

            {{--@include('includes.calendar')--}}

            {{--<div class="separator"></div>--}}

            {{--@include('includes.logos')--}}

        </div>
        <aside id="column-right" class="col-sm-3">
            <div class="main-info">
                <table class="borderless-custom" border="0">
                    <tbody>
                    <tr>
                        <td colspan="2" class="title-custom">@lang('gui.profile.aside.distance')</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td id="distance" colspan="2">@lang('helper.string.notAvailable')</td>
                        <td></td>
                    </tr>
                    @if($branches[0]->city != '')
                        <tr class="titled">
                            <td colspan="2" class="title-custom">@lang('gui.profile.aside.city')</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">{{ $branches[0]->city }}</td>
                            <td></td>
                        </tr>
                    @endif
                    @if (isset($profile->date_of_birth))
                        @if ($profile->date_of_birth != '0000-00-00')
                        <tr class="titled">
                            <td colspan="2" class="title-custom">@lang('gui.profile.aside.dateOfBirth')</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2" itemprop="birthDate">{{ date('d.m.Y',strtotime($profile->date_of_birth)) }}</td>
                            <td></td>
                        </tr>
                        @endif
                    @endif
                    @if (isset($profile->language))
                        @if ($profile->language != '')
                            <tr class="titled">
                                <td colspan="2" class="title-custom">@lang('gui.profile.aside.languages')</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2">{{  $profile->language }}</td>
                                <td></td>
                            </tr>
                         @endif
                    @endif

                    <tr class="titled">
                        <td colspan="2" class="title-custom">@lang('gui.profile.aside.businessContact')</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="address-custom">
                            @if ($profile->phone !== '')
                                <span itemprop="telephone">
                                    <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                    {{ \App\Helpers\StringHelper::formatPhone($profile->phone) }}
                                </span><br/>
                            @endif

                            @if (!empty($profile->fax))
                                <span itemprop="faxNumber">
                                    <i class="fa fa-fax fa-fw" aria-hidden="true"></i>
                                    {{ \App\Helpers\StringHelper::formatPhone($profile->fax) }}
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr class="titled">
                        <td colspan="2" class="title-custom">@lang('gui.profile.aside.businessAddress')</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="address-custom" itemprop="workLocation">
                            {{ $company[0]->CompanyName }}<br/>
                            @if ($company[0]->CompanyName != $branches[0]->name)
                                {{ $branches[0]->name }}<br/>
                            @endif

                            @if(!empty($branches[0]->address1))
                                {{ $branches[0]->address1}} {{ $branches[0]->address2}}<br />
                            @endif

                            @if(!empty($branches[0]->zip) && !empty($branches[0]->city))
                                {{ $branches[0]->zip }} {{ $branches[0]->city}}<br/>
                            @endif

                            @if(!empty($branches[0]->phone))
                                <span><i class="fa fa-phone fa-fw" aria-hidden="true"></i></span>
                                {{ \App\Helpers\StringHelper::formatPhone($branches[0]->phone)}}<br/>
                            @endif

                            @if(!empty($branches[0]->fax))
                                <span><i class="fa fa-fax fa-fw" aria-hidden="true"></i></span>
                                {{ \App\Helpers\StringHelper::formatPhone($branches[0]->fax )}}<br/>
                            @endif
                        </td>
                        <td></td>
                    </tr>
                    @if ( $profile->private_address_1 != '' )
                        <tr>
                            <td colspan="2" class="title-custom">

                                <span class="title">@lang('gui.profile.aside.privateAddress')</span>
                                <p>
                                    <strong>{{ $profile->firstname }} {{ $profile->lastname }}</strong><br/>

                                    {{ $profile->private_address_1 }}<br/>

                                    {{ $profile->private_address_2 }}<br/>

                                    {{ $profile->private_zip }} {{ $profile->private_city }}<br/>

                                    {{ \App\Helpers\StringHelper::formatPhone($profile->private_phone) }}<br/>

                                    {{ \App\Helpers\StringHelper::formatPhone($profile->private_mobile) }}<br/>

                                    {{ \App\Helpers\StringHelper::formatPhone($profile->private_fax) }}<br/>

                                </p>
                            </td>
                            <td></td>
                    @endif

                    </tr>
                    </tbody>
                </table>
                @include('includes.profile.map')
            </div>
        </aside>
        <div class="clear"></div>
    </div>
    @if (isset($_COOKIE['lat']))
        <script>
            var lat = {{ $_COOKIE['lat'] }};
            var lng = {{ $_COOKIE['lng'] }};
            var id = {{ $profile->id}};

                $.ajax({
                     type: "GET",
                     url: '/distance',
                     data: {lat:lat, lng:lng, id:id},
                     success: function( msg) {
                         $('#distance').html(msg);
                     }
                });
        </script>
    @endif
@endsection