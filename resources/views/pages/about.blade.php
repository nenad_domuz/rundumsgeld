@extends('layouts.default')

@section('title')
Über uns
@endsection

@section('content')
<h2>Was ist rundumsgeld.ch?</h2>
<p>
    Rundumsgeld.ch ist das Schweizer Portal, wenn es um die Suche nach Versicherungs- oder Finanzberatern geht.
    Unsere Kunden schätzen unsere Dienstleistung, weil sie auf rundumsgeld.ch schnell und fallbezogen Ihren Berater finden können.
    Sie können ohne Umwege den Berater kontaktieren und einen Termin vereinbaren.
    Egal ob es um Ihre Vorsorge, Ihre Autoversicherung, Ihre Anlagemöglichkeiten oder anderes geht,
    Sie werden bei rundumsgeld.ch stets den richtigen Berater finden.
</p>
<p>
    Dem gegenüber schätzen die bei uns registrierten Berater, das Sie von interessierten Kunden kontaktiert werden und dass Sie ein Portal zur Verfügung haben,
    auf welchem sie sich präsentieren können.
    Die Berater können hier Ihre Stärken und Kompetenzen einfach darstellen, sodass Kunden und Berater gut zueinander passen.
    Somit generiert rundumsgeld.ch eine Win-Win-Situation für Kunden und Berater.
</p>
<p>Rundumsgeld.ch ist damit das ideale Bindeglied zwischen Berater und Kunden.</p>
<h2>Wer steht hinter dem Portal?</h2>
<p>
    Das Portal wird von der ASVM Financial Services GmbH zur Verfügung gestellt.
    Die ASVM hat sich mit Ihren Dienstleistungen als Maklerplattform etabliert und sich im Laufe der Jahre darauf spezialisiert,
    Kunden, Berater, Makler, Versicherunggesellschaften und andere Finanzintermerdiäre zusammen zu bringen.
    Thomas Panzeri, der Geschäftsführer,
    verfolgt das Ziel einzigartige Plattformen mit Dienstleistungen und Produkten unseren Partnern wie auch unseren Kunden zur Verwendung zu stellen.
    Eines dieser Produkte ist rundumsgeld.ch.
</p>
<p>
    Die ASVM bietet Ihren Kundeninnen und Kunden danaben auch noch weitere insteressante Produkte und Dienstleistugnen an.
    Hervorzuheben ist hier sicherlich das Kundenverwaltungsprogramm CalWizz, welches es ermöglicht, Kunden besser zu betreuen,
    wie niemals zuvor - ein weiterer Gewinn für Kunden und Berater.
</p>
<!--<div class="image-accent image-border col-xs-12 col-sm-6 col-md-4 avatar-custom">-->
    <a href="https://info.calwizz.ch" target="_blank">
        <img class="img-fluid company-logo" src="{{ asset('images/calwizz.png') }}" alt="Calwizz-Logo">
    </a>
<p>
    Besuchen Sie uns doch auf der Infoseite unter <a href="https://info.calwizz.ch/" target="_blank">info.calwizz.ch</a>
    und erfahren Sie mehr über die Möglichkeiten von CalWizz.
    Alle Dienstleistungen der AVSM finden Sie naütrlich auf unserer Homepage unter <a href="https://asvm.ch/" target="_blank">asvm.ch</a>.
</p>
<a href="https://asvm.ch" target="_blank">
    <img class="img-fluid company-logo" src="{{ asset('images/asvm.png') }}" alt="ASVM-Logo">
</a>
<p>Sie können uns auch gerne über unser <a href="/kontakt" target="_blank">Kontaktformular</a>,
    telefonisch unter der Nummer 043 299 60 00 oder per Email an <a href="mailto:info@asvm.ch" target="_blank">info@asvm.ch</a> kontaktieren.
</p>
<h2>Der Geschäftsführer</h2>
<div class="row col-xs-12 col-md-6 col-lg-6">
    <p>
        Thomas Panzeri kann auf langjährige Erfahrungen in der Finanzbranche zurückblicken.
        Zunächst sammelte er diese in der Ausbildung und Führung von Mitarbeitern.
        In jener Position kam die Erkenntnis, welche bis zum heutigen Tag in seinem Handeln ersichtlich ist,
        dass gewisse Strukturen in der Finanzbranche verbesserungswürdig sind.
    </p>
    <p>
        Anschliessend arbeitete er unter anderem als Finanzplaner an vorderster Front und übernahm später die Verkaufsleitung inkl.
        Mitarbeiterführung in einem angesehenen Finanzunternehmen.
    </p>
    <p>
        Nach insgesamt zehn Jahren an der Kundenfront wechselte er das Terrain,
        um Maklern mit bestem Wissen und Gewissen zur Seite zu stehen – und das mit Erfolg!!
    </p>
    <a rel="me" href="https://www.linkedin.com/in/panzeri" target="_blank">
        <i class="fa fa-linkedin"></i>
    </a>
    <a rel="me" href="http://www.xing.com/profile/Thomas_Panzeri" target="_blank">
        <i class="fa fa-xing"></i>
    </a>
    <a rel="me" href="https://plus.google.com/105571631257826653365?rel=author" target="_blank">
        <i class="fa fa-google-plus"></i>
    </a>
</div>
<div class="col-xs-12 col-md-6 col-lg-6 image-accent">
    <a href="https://asvm.ch/%C3%BCber-asvm/team/thomas-panzeri.html" target="_blank">
        <img class="img-fluid image-border" src="{{ asset('/images/thomas-panzeri.jpg') }}" alt="Thomas Panzeri" />
    </a>
</div>
@endsection
