@extends('layouts.default')

@section('title')
Versicherungsberatung
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <p>
                Das Kernstück
                jeder Versicherungsberatung ist die Expertise der Berater. Um ideal abgesichert
                zu sein ist es deshalb wichtig, dass Sie kompetent beraten werden. Das geht am
                besten mit einem Service der auf Ihre Bedürfnisse abgestimmt ist.<br>Kompetente
                und gut ausgebildete Versicherungsberater haben eine Expertise, mit welcher Sie
                die unterschiedlichsten Problemstellungen für Sie im Assekuranzbereich lösen
                können.</p>
            <p>
                Sie kennen die
                Branche, haben ein weitgespanntes Netzwerk und entsprechende Erfahrung. So
                können Sie Ihnen als Kunde einen Service bieten, welcher für Sie einen Mehrwert
                generiert. Sie stehen Ihnen bei der Auswahl der passenden Versicherung zur
                Seite. Wodurch Sie eine Abdeckung haben, welche mit Ihren persönlichen Zielen
                und Ihrem Sicherheitsbewusstsein übereinstimmt. Sie sind dann in einem
                Schadensfall oder bei einer Krankheit ideal abgedeckt und müssen sich keinerlei
                Gedanken machen. Des Weiteren sparen Sie in den meisten Fällen Geld, da der
                Berater die nötigen von den unnötigen Versicherungen besser unterscheiden kann
                und somit Ihr Assekuranzportfolio optimal gestaltet.</p>
            <h2>
                Vorteile einer Versicherungsberatung</h2>
            <p>
                Grundsätzlich ist es immer zu empfehlen, mit einem
                Versicherungsberater zusammen Ihre Versicherung zu erstellen oder falls diese
                schon besteht nochmals anzupassen. Hinzu kommt, dass die ganze Beratung
                kostenlos ist. Wieso also nicht, wenn Sie im Nachhinein besser dastehen als vor
                der Beratung?</p>
            <p>
                Ein
                Versicherungsberater bringt mit Sicherheit Vorteile mit sich, welche da wären:</p>
            <ul class="fa-ul">
                <li><i class="fa-li fa fa-check"></i>
                    Er erkennt, ob Sie unter- über- oder doppelt versichert sind. Eine solche
                    Erkenntnis kann nur ein Berater mit sich bringen!
                </li>
                <li><i class="fa-li fa fa-check"></i>
                    Des Weiteren kann er Sie auf
                    mögliche Sparpotentiale aufmerksam machen und diese direkt optimieren.
                </li>
                <li><i class="fa-li fa fa-check"></i>
                    Dazu kann
                    ein Berater die Versicherung individuell auf Sie anpassen. Somit haben Sie dann
                    eine massgeschneiderte Versicherung und können wohlbehütet durchs Leben
                    gehen.
                </li>
                <li><i class="fa-li fa fa-check"></i>
                    Was sicherlich noch hervor zu heben ist, dass ein Versicherungsberater
                    Ihnen viel Zeit, Probleme, Aufwand und somit auch viel Geld einsparen kann.
                </li>
            </ul>
            <h2>Nach welchen Kriterien Sie Ihren Berater suchen könnnen</h2>
            <p>
                Das
                Rundumsgeld-Team hat sich zur Aufgabe gemacht, die Versicherungsmakler der
                Schweiz an einem Ort zusammenzubringen. Dies ermöglicht Ihnen, aus einer grossen
                Masse an Beratern den für Sie passenden auszuwählen. Ohne mühsame Wühlerei.</p>
            <ul class="fa-ul">
                <li><i class="fa-li fa fa-check"></i>Versicherung</li>
                <li><i class="fa-li fa fa-check"></i>Berater</li>
                <li><i class="fa-li fa fa-check"></i>Region</li>
                <li><i class="fa-li fa fa-check"></i>Sprache</li>
            </ul>

@include('modules.userActionButton')
        </div>
    </div>
@endsection