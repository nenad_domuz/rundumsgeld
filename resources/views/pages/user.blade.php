@extends('layouts.default')

@section('title')
Hallo, {{ $userdata->firstname }} {{ $userdata->lastname }}
@endsection
@section('content')
<h1>
</h1>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
    <h2>@lang('gui.user.profilePicture')</h2>
    @include('pages.picture')
</div>
<div class="col-xs-1 col-sm-12 col-md-4 col-lg-1"></div>
<div class="col-xs-10 col-sm-12 col-md-4 col-lg-8">
    <form method="POST" role="form" action="{{ url('/user/update') }}">
        <h2>@lang('gui.user.profileInformation')</h2>
        {{ csrf_field() }}
        <input type="hidden" id="lat" name="lat" value="{{ $profiledata[0]->latitude }}"/>
        <input type="hidden" id="lng" name="lng" value="{{ $profiledata[0]->longitude }}"/>
        @include('errors.errorMessages')
        <div class="col-lg-6">
            <div class="form-group">
                <label for="phone"><h3>@lang('gui.user.phone.title')</h3><small>&nbsp;&nbsp;@lang('gui.user.phone.appearsAs')</small></label>
                <input type="text" class="form-control" name="phone" id="phone" value="{{ $profiledata[0]->phone }}">
                {{-- Error template for each input field --}}
                {{--@if ($errors->has('phone'))--}}
                    {{--<span class="help-block">--}}
                        {{--<strong>{{ $errors->first('phone') }}</strong>--}}
                    {{--</span>--}}
                {{--@endif--}}
            </div>
            <div class="form-group">
                <label for="fax"><h3>Fax</h3><small>&nbsp;&nbsp;@lang('gui.user.fax.appearsAs')</small></label>
                <input type="text" class="form-control" name="fax" id="fax" value="{{ $profiledata[0]->fax }}">
            </div>

            <div class="form-group">
                <label for="mobile"><h3>Mobile</h3><small>&nbsp;&nbsp;@lang('gui.user.phone.appearsAs')</small></label>
                <input type="text" class="form-control" name="mobile" id="mobile" value="{{ $profiledata[0]->mobile }}">
            </div>
            <div class="form-group">
                <label for="aboutMe"><h3>@lang('gui.profile.aboutMe')</h3></label>
                <textarea class="form-control" name="aboutMe" id="aboutMe" placeholder="" rows="8" autocomplete="off" spellcheck="false">{{ trim($profiledata[0]->about_me) }}</textarea>
            </div>
            <div class="form-group">
                <label for="dob"><h3>@lang('gui.profile.aside.dateOfBirth')</h3></label>
                <input type="text" class="form-control" name="dob" id="dob" value="{{ $profiledata[0]->date_of_birth }}">
            </div>
            <div class="form-group">
                <fieldset disabled>
                    <label for="disabledTextInput"><h3>@lang('gui.user.finma.title')</h3><small>&nbsp;@lang('gui.user.finma.cannotChange')</small></label>
                    <input type="text" id="disabledTextInput" class="form-control" placeholder="Disabled input" value="{{ $profiledata[0]->finma_registration_number }}">
                </fieldset>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">@lang('gui.user.button.updateProfile')</button>
                <a target="_blank" class="btn btn-default" href="{{ \App\Helpers\StringHelper::createProfilePageUrl($profiledata[0]->CompanyName, $profiledata[0]->lastname, $profiledata[0]->firstname, $profiledata[0]->id) }}">@lang('gui.user.button.viewProfile')</a>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <label for="hobbies"><h3>@lang('gui.profile.hobbies')</h3></label>
                <textarea class="form-control" name="hobbies" id="hobbies" placeholder="" rows="2" autocomplete="off" spellcheck="false">{{ trim($profiledata[0]->hobbies) }}</textarea>
            </div>
            <div class="form-group">
                <label for="training"><h3>@lang('gui.profile.training')</h3></label>
                <textarea class="form-control" name="training" id="training" placeholder="" rows="2" autocomplete="off" spellcheck="false">{{ trim($profiledata[0]->training) }}</textarea>
            </div>
            <div class="form-group">
                <label for="skills"><h3>@lang('gui.profile.skills')</h3></label>
                <textarea class="form-control" name="skills" id="skills" placeholder="" rows="2" autocomplete="off" spellcheck="false">{{ trim($profiledata[0]->skills) }}</textarea>
            </div>
            <div class="form-group">
                <label for="education"><h3>@lang('gui.profile.education')</h3></label>
                <textarea class="form-control" name="education" id="education" placeholder="" rows="2" autocomplete="off" spellcheck="false">{{ trim($profiledata[0]->education) }}</textarea>
            </div>
            <div class="form-group">
                <label for="language"><h3>@lang('gui.profile.aside.languages')</h3></label>
                <textarea class="form-control" name="language" id="language" placeholder="" rows="2" autocomplete="off" spellcheck="false">{{ trim($profiledata[0]->language) }}</textarea>
            </div>
        </div>
        <div class="col-md-12 col-lg-12" id='map_canvas' style="height: 300px">
        </div>
    </form>
</div>
<script type="text/javascript">
    $.datepicker.setDefaults( $.datepicker.regional[ "de" ] );

    $( "#dob" ).datepicker({
        altField: "#dob",
        altFormat: "yy-mm-dd",
        changeYear: true,
        changeMonth: true,
        yearRange: '1920:2016',
        maxDate: "-17y"
    });
</script>
<script>
    function initMap()
    {
       var map = new google.maps.Map(document.getElementById('map_canvas'), {
           zoom: 7,
           center: new google.maps.LatLng( @if ($profiledata[0]->latitude) {{ $profiledata[0]->latitude }} @elseif ($profiledata[0]->lat) {{ $profiledata[0]->lat }} @else {{ 47.3774337 }} @endif, @if ($profiledata[0]->longitude) {{ $profiledata[0]->longitude }} @elseif ($profiledata[0]->lng) {{ $profiledata[0]->lng }} @else {{ 8.4666757 }} @endif ),
           mapTypeId: google.maps.MapTypeId.ROADMAP
       });

       var myMarker = new google.maps.Marker({
           position: new google.maps.LatLng(@if ($profiledata[0]->latitude) {{ $profiledata[0]->latitude }} @elseif ($profiledata[0]->lat) {{ $profiledata[0]->lat }} @else {{ 47.3774337 }} @endif, @if ($profiledata[0]->longitude) {{ $profiledata[0]->longitude }} @elseif ($profiledata[0]->lng) {{ $profiledata[0]->lng }} @else {{ 8.4666757 }} @endif),
           draggable: true
       });

       google.maps.event.addListener(myMarker, 'dragend', function (evt) {
           document.getElementById('lat').value = evt.latLng.lat().toFixed(5);
           document.getElementById('lng').value = evt.latLng.lng().toFixed(5);
       });

       map.setCenter(myMarker.position);
       myMarker.setMap(map);
    }
</script>
@endsection
