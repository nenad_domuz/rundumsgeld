@extends('layouts.default')

@section('title')
Finanzplanung
@endsection

@section('content')
<h2>
    Was ist Finanzberatung</h2>
<p>
    Im Volksmund versteht man unter Finanzberatung direkt Versicherungen verkaufen. Es kommt ein
    Anzugträger zu einem ins Wohnzimmer und will einem irgendetwas andrehen. Man
    weiss nie genau, was da wirklich los ist oder ob das, was der Fremde einem da
    anbietet, sinnvoll ist oder nur Verkaufstaktik oder gar eine Abzocke.</p>

<p>In unserer täglichen Praxis lässt sich die Finanzberatung in drei Teile
    spalten.</p>
<ul class="fa-ul">
    <li><i class="fa-li fa fa-check"></i>Geldanlage</li>
    <li><i class="fa-li fa fa-check"></i>Altersvorsorge </li>
    <li><i class="fa-li fa fa-check"></i>Baufinanzierung oder Kredite</li>
</ul>
<h3>Geldanlage</h3>
<p>
    Die Geldanlage ist
    schon seit mehreren Jahrzehnten ein wichtiger Aspekt der Finanzberatung. Was
    mache ich mit meinem Geld? Wie bringt es mir am meisten ein in der Zukunft? Will
    ich damit spekulieren oder lege ich es für die Altersvorsorge zurück? Die
    meisten Menschen entscheiden sich für die Altersvorsorge und damit geht es
    gleich weiter mit Punkt zwei.</p>
<h3>
    Altersvorsorge</h3>
<p>
    Altersvorsorge
    bedeutete über mehrere Jahre hinweg hauptsächlich Versicherungsthemen. Das hat
    sich mittlerweile geändert. Heute geht es darum, ohne einen zu teuren
    Versicherungsmantel eine Geldanlage so zu strukturieren, dass man im Alter
    genügend flüssige Mittel zur Verfügung hat um damit zu Leben.</p>
<h3>
    Baufinanzierung</h3>
<p>
    Dies ist sogleich
    die wichtigste Entscheidung, die viele Menschen in Ihrem Leben im
    wirtschaftlichen Bereich treffen. Und dafür ist man früher schlicht zur Hausbank
    gegangen, im Glauben dass die Hausbank einen und die Familie kenne. Die Hausbank
    fände schon die richtige Lösung. Wir wissen heute aber, dass viele Aspekte dort
    geschäftsgetrieben sind. Das heisst, man möchte dringend einen Kredit haben und
    insofern ist die Beratung vermutlich gerade nicht in der unabhängigen Form, die
    wir als Honorarberater das richtig empfinden. Darum sind für uns alle drei
    Aspekte, also Geldanlage, Altersvorsorge und Baufinanzierung, wichtige
    Teilaspekte, die wir in unserer Finanzberatung auf Honorarbasis anbieten</p>
<p>
    In der Geldanlage
    setzen gute Berater ausschliesslich auf wissenschaftliche Erkenntnisse. Zudem
    halten Berater Meinungen darüber, ob gewisse Märkte und welche Währungen besser
    oder schlechter sind. Ein guter Berater wird Ihnen sagen, dass er nicht glaubt
    die Zukunft vorherzusehen oder sagen zu können welche Märkte in einem Jahr gut
    oder schlecht sind.</p>
<h2>
    Unser Tipp wie Sie einen guten Finanzberater erkennen</h2>
<p>
    Bei einer
    Finanzberatung geht der Berater wie Ihr Hausarzt vor. Sie werden zuerst
    gründlich untersucht d.h. Ihre Finanzielle Situation und Ihre Wünsche werden
    analysiert. Dadurch kann der Finanzberater die richtige Prognose stellen.
    Wenn Sie Ihr Geld anlegen möchten oder Ihr Vermögen verwalten lassen wollen,
    empfiehlt es sich einen Berater zu beauftragen, dem Sie vertrauen und dessen
    Entscheidungen Sie nachvollziehen können. Eine seriöse Finanzberatung
    beschränkt sich nicht nur auf Tipps und Empfehlungen bezüglich Ihres
    Anlagevermögens, sondern sie zieht auch die Themen der persönlichen
    Altersversorgung und Vorsorge in die Beratung hinein.</p>
@include('modules.userActionButton')
@endsection