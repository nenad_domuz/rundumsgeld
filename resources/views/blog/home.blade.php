@extends('layouts.default')
@section('title')
@if (strpos(Request::url(),'user'))
    @lang('blog.posts.yourPosts')
@else
    @lang('blog.posts.page')
@endif
@endsection
@section('content')
    @include('errors.errorMessages')
@if (empty($posts))
    <p>@lang('noPosts.title')</p>
@else
    <div class="negative-10">
        <div class="col-xs-12 col-md-12">
            @foreach( $posts as $post )
                <div class="list-group">
                    <div class="list-group-item">
                        <h2 style="display: inline-block">
                            <a href="{{ url('/blog/' . $post->slug) }}">{{ $post->title }}</a>
                        </h2>
                        @if(isset($action) && $action[1] == 'userPosts')
                            @if($post->active == 1)
                                <span class="label label-info">@lang('blog.post.label.approved')</span>
                            @endif
                            @if($post->active == 0)
                                <span class="label label-danger">@lang('blog.post.label.waiting')</span>
                            @endif
                        @endif
                            {{--@if(Auth::user() && ($post->user_id == Auth::user()->id || Auth::user()->isAdmin()))--}}
                            {{--@if($post->active == 1)--}}
                                {{--<a class="btn btn-primary" style="float: right; margin-top: 10px;" href="{{ url('/blog/edit/'.$post->slug) }}">@lang('blog.nav.editPost')</a>--}}
                            {{--@else--}}
                                {{--<a class="btn btn-primary" style="float: right; margin-top: 10px" href="{{ url('/blog/edit/'.$post->slug) }}">@lang('blog.nav.editDraft')</a>--}}
                            {{--@endif--}}
                        {{--@endif--}}
                        <p>
                           {{ $post->created_at->format('d.m.Y, G:H') }} @lang('blog.post.by')
                            <i>
                                @if(isset($post->author->profile))
                                    <a target="_blank" href="{{ \App\Helpers\StringHelper::createProfilePageUrl($post->author->profile->branches[0]->company->name, $post->author->lastname, $post->author->firstname, $post->author->profile->id) }}">
                                    {{ $post->author->profile->firstname }}&nbsp;{{ $post->author->profile->lastname }}
                                    </a>
                                @else
                                    {{ $post->author->firstname . PHP_EOL . $post->author->lastname }}
                                @endif
                            </i>
                        </p>
                      </div>
                      <div class="list-group-item">
                        <article>
                          {!! str_limit($post->body, 1500, $end = '... <br/><br/><a href=' . url("/blog/" . $post->slug) . '>...' . trans('gui.blog.readMore') . '</a>') !!}
                        </article>
                      </div>
                </div>
            @endforeach
        </div>
    </div>
    {!! $posts->render() !!}
@endif
@endsection