@extends('layouts.default')
@section('title')
@lang('gui.blog.edit')
@endsection
@section('content')
@include('errors.errorMessages')
<script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
  tinymce.init({
    language: 'de',
    selector : "textarea",
    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
  }); 
</script>
<form method="post" action='{{ url("/blog/update") }}'>
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" name="post_id" value="{{ $post->id }}{{ old('post_id') }}">
  <div class="form-group">
    <input required="required" placeholder="@lang('blog.post.create.title.placeholder')" type="text" name="title" class="form-control" value="@if(!old('title')){{$post->title}}@endif{{ old('title') }}"/>
  </div>
  <div class="form-group">
    <textarea name='body'class="form-control">
      @if(!old('body'))
        {!! $post->body !!}
      @endif

      {!! old('body') !!}
    </textarea>
  </div>

  <input type="submit" name="publish" class="btn btn-success" value="@lang('blog.post.button.update')"/>
  {{--<input type="submit" name="publish" class="btn btn-success" value="@lang('blog.post.button.publish')"/>--}}
  {{--<input type="submit" name='save' class="btn btn-default" value ="@lang('blog.post.button.saveDraft')" />--}}

  {{--@if(count($post->comments) == 0)--}}
    {{--<a href="{{  url('/blog/delete/' . $post->id . '?_token=' . csrf_token()) }}" class="btn btn-danger">@lang('blog.post.button.delete')</a>--}}
  {{--@endif--}}

</form>
@endsection