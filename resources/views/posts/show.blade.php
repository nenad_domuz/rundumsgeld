@extends('layouts.default')

@section('title')
    @if($post)
        {{ $post->title }}
        <p>
            <small>
                <small>
                    {{ $post->created_at->format('M d, Y \a\t h:i a') }} @lang('blog.post.by')

                    @if(isset($post->author->profile))
                        <a target="_blank" href="{{ \App\Helpers\StringHelper::createProfilePageUrl($post->author->profile->branches[0]->company->name, $post->author->lastname, $post->author->firstname,  $post->author->profile->id) }}">
                            {{ $post->author->profile->firstname }}&nbsp;{{ $post->author->profile->lastname }}
                        </a>
                    @else
                        {{ $post->author->firstname }}&nbsp;{{ $post->author->lastname }}
                    @endif

                </small>
            </small>
        </p>
    @else
        @lang('blog.blog.page.notExists')
    @endif
@endsection

@section('title-meta')
    <p>{{ $post->created_at->format('M d, Y \a\t h:i a') }} @lang('blog.post.by') <a href="{{ url('/blog/user/' . $post->user_id) }}">{{ $post->author->firstname }}</a></p>
@endsection
{{--@if(Auth::user() && ($post->user_id == Auth::user()->id))--}}
    {{--@if($post->active != 0)--}}
        {{--<a class="btn" style="float: right" href="{{ url('/blog/edit/' . $post->slug) }}">Edit Post</a>--}}
    {{--@else--}}
        {{--<a class="btn" style="float: right" href="{{ url('/blog/edit/' . $post->slug) }}">Edit Draft</a>--}}
    {{--@endif--}}
{{--@endif--}}
@section('content')

@include('errors.errorMessages')

@if($post)
    <div>
        {!! wordwrap($post->body, 75, PHP_EOL, true)!!}
    </div>
    <div>
        <h2>@lang('blog.post.leaveComment')</h2>
    </div>
  @if(Auth::guest())
    <p>@lang('blog.post.loginToComment')</p>
  @else
    <div class="panel-body">
      <form method="post" action="{{ url('/blog/comment/add') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="post_id" value="{{ $post->id }}">
        <input type="hidden" name="slug" value="{{ $post->slug }}">
        <div class="form-group">
          <textarea required="required" placeholder="@lang('blog.post.enterComment')" name="body" class="form-control"></textarea>
        </div>
        <input type="submit" name='post_comment' class="btn btn-success" value="@lang('blog.post.postComment')" />
      </form>
    </div>
  @endif
  <div>
  @if($comments)
          <ul style="list-style: none; padding: 0">
    @foreach($comments as $comment)
        <li class="panel-body">
          <div class="list-group">
            <div class="list-group-item">
              <h3>{{ $comment->firstname }}&nbsp;{{ $comment->lastname }}</h3>
              <p>{{ date('M d, Y \a\t h:i a', strtotime($comment->created_at)) }}</p>
            </div>
            <div class="list-group-item">
              <p>{{ $comment->body }}</p>
            </div>
          </div>
        </li>
      @endforeach
    </ul>
    @endif
  </div>
@else
    404 error
@endif
@endsection