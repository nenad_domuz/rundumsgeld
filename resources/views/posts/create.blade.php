@extends('layouts.default')
@section('title')
@lang('gui.blog.create')
@endsection

@section('content')

@include('errors.errorMessages')

  <script type="text/javascript" src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
  <script type="text/javascript">
    tinymce.init({
      language: 'de',
      selector : "textarea",
      plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
      toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    });
  </script>
  <form action="{{ url('/blog/new-post') }}" method="post" novalidate>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group">
      <input required="required" value="{{ old('title') }}" placeholder="@lang('blog.post.create.title.placeholder')" type="text" name="title" class="form-control" autocomplete="off" />
    </div>
    <div class="form-group">
      <textarea title="body" name="body" class="form-control">{{ old('body') }}</textarea>
    </div>
    <input type="submit" name="publish" class="btn btn-success" value="@lang('blog.post.button.publish')"/>
    {{--<input type="submit" name="save" class="btn btn-default" value="@lang('blog.post.button.saveDraft')" />--}}
  </form>
@endsection