@if (!isset($_COOKIE['lat']))
    <script type='text/javascript'>
        function initMap() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var lat = position.coords.latitude;
                    var lng = position.coords.longitude;
                    document.cookie = "lat=" + lat + "; expires=Fri, 3 Aug 2018 20:47:11 UTC; path=/";
                    document.cookie = "lng=" + lng + "; expires=Fri, 3 Aug 2018 20:47:11 UTC; path=/";
                });
            }
        }
    </script>
@endif