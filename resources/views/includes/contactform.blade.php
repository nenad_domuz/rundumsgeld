@include('errors.errorMessages')
<form role="form" method="POST" id="contact-form" action="{{ action('ContactController@send') }}">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="control-label" for="contact-name">@lang('gui.contact.name')</label>
        <input type="text" class="form-control form-required" name="contactName" id="contactName" value="{{ old('contactName') }}" placeholder="@lang('gui.contact.name.placeholder')" autocomplete="off">
    </div>
    <div class="form-group">
        <label class="control-label" for="contact-email">@lang('gui.contact.email')</label>
        <input type="email" class="form-control form-required" name="contactEmail" id="contactEmail" value="{{ old('contactEmail') }}" placeholder="@lang('gui.contact.email.placeholder')" autocomplete="off">
    </div>
    <div class="form-group">
        <label class="control-label" for="contact-website">@lang('gui.contact.website')</label>
        <input type="text" class="form-control" name="contactWebsite" id="contactWebsite" value="{{ old('contactWebsite') }}" placeholder="@lang('gui.contact.website.placeholder')" autocomplete="off">
    </div>
    <div class="form-group">
        <label class="control-label" for="contact-message">@lang('gui.contact.message')</label>
        <textarea class="form-control form-required" name="contactMessage" id="contactMessage" placeholder="@lang('gui.contact.message.placeholder')" rows="7" required autocomplete="off"></textarea>
    </div>
    <div class="form-group">
        <p class="form-buttons">
            <button type="submit" class="btn btn-accent">@lang('gui.contact.send')</button>
            <input type="reset" class="btn btn btn-light" value="@lang('gui.contact.reset')">
        </p>
    </div>
</form>