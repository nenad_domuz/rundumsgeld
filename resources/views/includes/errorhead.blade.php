<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0 maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@lang('gui.head.title') | @yield('title')</title>

<!-- Fonts integration -->
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300italic,300,400italic,700,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>

<link href="{{ asset('css/pe-icon-7-stroke.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/pe-icon-7-stroke-helper.css') }}" rel="stylesheet" type="text/css"/>

<!-- CSS import -->
<link href="{{ asset('js/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/rundumsgeld.css') }}" rel="stylesheet">
<link href="{{ asset('css/desktop.css') }}" rel="stylesheet">
<link href="{{ asset('css/screens.css') }}" rel="stylesheet">
<link href="{{ asset('css/mobile.css') }}" rel="stylesheet">
<link href="{{ asset('js/slick/slick.css') }}" rel="stylesheet">

<!-- Favicon import -->
<link href="{{ asset('/favicon.png') }}" rel="icon" />

<style>
    html, body {
        height: 100%;
    }

    body {
        margin: 0;
        padding: 0;
        width: 100%;
        color: #333;
        display: table;
        font-weight: 100;
        font-family: 'Roboto';
        background-position: top;
    }

    img {
        margin-bottom: 36px;
    }

    .container-fluid {
        text-align: center;
        display: table-cell;
        vertical-align: middle;
    }

    .content {
        text-align: center;
        display: inline-block;
    }

    .title {
        font-size: 36px;
        margin-bottom: 40px;
    }

    ul {
        list-style: none;
        padding-top: 30px;
    }

    ul li {
        text-align: left;
        line-height: 1;
    }
    p > a {
        font-weight: 200;
    }
    @media only screen
    and (min-device-width: 320px)
    and (max-device-width: 568px)
    and (-webkit-min-device-pixel-ratio: 2) {
        body {
            height: auto;
            min-height: 0;
        }
        a img {
            width: 100%;
        }
        .container-fluid {
            vertical-align: top;
        }
        .main ul li {
            text-align: center;
        }
        .title,
        .main ul li a {
            font-size: 26px;
        }
    }
</style>