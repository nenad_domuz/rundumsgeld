<div class="map-holder details-map">
    <div class="map" itemscope itemtype="http://schema.org/Place">
        <div id="map_canvas" itemprop="hasMap"></div>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript">
            var map = null;
            var markerArray = []; //create a global array to store markers
            @if($profile->longitude == Null && !empty($branches[0]->longitude))
            var locations = [
                @foreach ($branches as $key => $branch)
                    ["{{ \App\Helpers\StringHelper::transformUrl($branch->Address1) }} {{  \App\Helpers\StringHelper::transformUrl($branch->Address2) }} {{  \App\Helpers\StringHelper::transformUrl($branch->City) }}", {{ $branch->latitude }}, {{ $branch->longitude }}, {{ $key }}, '/images/pin.png'],
                @endforeach
            ];
            @else
            var locations = [
                    ["{{ $profile->firstname }} {{ $profile->lastname }}", @if ($profile->latitude != 0) {{ $profile->latitude }} @else 47.3774337 @endif, @if ($profile->longitude != 0) {{ $profile->longitude }} @else 8.4666757 @endif, 0, '/images/pin_red.png'],
            ];
            @endif
            function initMap() {
                var styles = [
                    {"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},
                    {"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},
                    {"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]},
                    {"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},
                    {"lightness":24}]},
                    {"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
                    {"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},
                    {"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]}
                ];

                var styledMap = new google.maps.StyledMapType(styles,
                        {name: "Styled Map"});

                var myOptions = {
                    zoom: 6,
                    center: new google.maps.LatLng(@if ($branches[0]->latitude) {{ $branches[0]->latitude }} @elseif ($profile->latitude) {{ $profile->latitude }} @else {{ 47.3774337 }} @endif, @if ($branches[0]->longitude) {{ $branches[0]->longitude }} @elseif ($profile->longitude) {{ $profile->longitude }} @else {{ 8.4666757 }} @endif ),
                    mapTypeControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }

                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

                map.mapTypes.set('map_style', styledMap);
                map.setMapTypeId('map_style');

                google.maps.event.addListener(map, 'click', function() {
                    infowindow.close();
                });

                // Add markers to the map
                // Set up markers based on the number of elements within the myPoints array
                for (var i = 0; i < locations.length; i++) {
                    createMarker(new google.maps.LatLng(locations[i][1], locations[i][2]),locations[i][0], locations[i][3], locations[i][4]);
                }

//                map.addMarkers(markerArray, true);
            }

            var infowindow = new google.maps.InfoWindow({
                size: new google.maps.Size(150, 50)
            });

            function createMarker(latlng, myTitle, myNum, myIcon) {
                var contentString = myTitle;
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: myIcon,
                    zIndex: Math.round(latlng.lat() * -100000) << 5,
                    title: myTitle
                });

                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                });

                markerArray.push(marker); //push local var marker into global array
            }

        </script>
    </div>
</div>