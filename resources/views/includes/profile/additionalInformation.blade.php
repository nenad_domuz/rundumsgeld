<div class="additional" itemscope itemtype ="http://schema.org/Service">
    <div class="row">
        <div class="col-md-12">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 clear-after">
            @if (\App\Helpers\DataBaseHelper::GetProductOverviewListForProfile($profile->id))
                <p class="title-custom">Products</p><br/>
                <ul class="additional-list fa-ul" itemprop="hasOfferCatalog" itemscope itemtype="http://schema.org/OfferCatalog">
                    @foreach(\App\Helpers\DataBaseHelper::GetProductOverviewListForProfile($profile->id) as $product)
                        <li itemprop="itemListElement">
                            <i class="fa-pull-right fa-li fa fa-check"></i>
                            {{ $product->ProductName }} - {{$product->InsuranceCompanyName}}
                        </li>
                    @endforeach
                </ul>
            @else
                <p class="title-custom">@lang('gui.profile.additionalInformation.none')</p>
            @endif
            </div>
        </div>
    </div>
</div>