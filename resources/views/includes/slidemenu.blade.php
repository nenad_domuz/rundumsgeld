<div id="responsive-menu">
    <div class="menu-wrap">
        <nav class="main">
            <ul>
                <li><a href="/versicherungsberatung">Versicherungsberatung</a></li>
                <li><a href="/finanzplanung">Finanzplanung</a></li>
                <li><a href="/kreditberatung">Kreditberatung</a></li>
                <li><a href="/finanz-blog">Finanz-Blog</a></li>
            </ul>
        </nav>
    </div>
</div>