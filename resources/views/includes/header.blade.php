<div class="container" xmlns:.Auth="http://www.w3.org/1999/xhtml">
    <div class="col-xs-12">
        <div class="logo">
            <a href="{{ url('/') }}">
                <img src="{!! asset('images/logo-header.png') !!}" alt="Rundumsgeld" title="Rundumsgeld" />
            </a>
        </div> 
        <nav class="main">
            <span class="toggle-nav"></span>
            <ul>
                @if (Auth::guest())
                    <li><a href="{{ url('/versicherungsberatung') }}">Versicherungsberatung</a></li>
                    <li><a href="{{ url('/finanzplanung') }}">Finanzplanung</a></li>
                    <li><a href="{{ url('/kreditberatung') }}">Kreditberatung</a></li>
                    <li><a href="{{ url('/blog') }}">Finanz-Blog</a></li>
                @else
                    @if(Auth::user()->isAdmin())
                        <li><a href="{{ url('/admin') }}">Admin</a></li>
                        @if(!empty(Auth::user()->profile->user_id) && Auth::user()->id === Auth::user()->profile->user_id)
                            <li><a href="{{ url('/user') }}">@lang('gui.menu.yourProfile')</a></li>
                        @endif
                    @endif

                    @if(Auth::user()->role == 'agent')
                        <li><a href="{{ url('/user') }}">@lang('gui.menu.yourProfile')</a></li>
                    @endif

                    <li><a href="/blog">@lang('gui.menu.financeBlog')</a></li>

                    @if (Auth::user()->canPost())
                        <li><a href="{{ url('/blog/user/' . Auth::id() . '/posts') }}">@lang('gui.menu.myPosts')</a></li>
                        <li><a href="{{ url('/blog/new-post') }}">@lang('gui.menu.addPost')</a></li>
                    @endif
                    <li><a href="{{ url('/logout') }}">@lang('gui.menu.logout')</a></li>
                @endif
            </ul>
            <script>
                $(document).ready(function(){
                    var activeurl = window.location.pathname;
                    var activeUserurl = window.location.href;
                    $('a[href="'+activeurl+'"]').addClass('current');
                    $('a[href="'+activeUserurl+'"]').addClass('current');

                    $('.toggle-nav').click(function(){
                        $(this).toggleClass('active');
                        $('nav.main > ul').toggleClass('open');
                    });
                });
            </script>
        </nav>
    </div>
</div>

