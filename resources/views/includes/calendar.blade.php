<span class="time-to-book">Click on any of the times you prefer to book <i class="fa fa-info"></i></span>
<h2>Booking times</h2>
<div class="slick-times">
    <div class="times">
        <div class="day">
            <div class="day-title">Monday<br/>
                <span class="date">18.01.2016</span>
            </div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Tuesday<br/>
                <span class="date">19.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Wednesday<br/>
                <span class="date">20.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Thursday<br/>
                <span class="date">21.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Friday<br/>
                <span class="date">22.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Monday<br/>
                <span class="date">18.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Tuesday<br/>
                <span class="date">19.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Wednesday<br/>
                <span class="date">20.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Thursday<br/>
                <span class="date">21.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Friday<br/>
                <span class="date">22.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Monday<br/>
                <span class="date">18.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Tuesday<br/>
                <span class="date">19.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Wednesday<br/>
                <span class="date">20.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Thursday<br/>
                <span class="date">21.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
        <div class="day">
            <div class="day-title">Friday<br/>
                <span class="date">22.01.2016</span></div>
            <div class="day-row">
                <div class="day-time">10:00</div>
                <div class="day-time">10:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">11:00</div>
                <div class="day-time">11:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">12:00</div>
                <div class="day-time day-available">12:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">13:00</div>
                <div class="day-time">13:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">14:00</div>
                <div class="day-time">14:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">15:00</div>
                <div class="day-time day-available">15:30</div>
            </div>
            <div class="day-row">
                <div class="day-time">16:00</div>
                <div class="day-time">16:30</div>
            </div>
            <div class="day-row">
                <div class="day-time day-available">17:00</div>
                <div class="day-time day-available">17:30</div>
            </div>
        </div>
    </div>
</div>