<div class="map-holder search-map" style="height: 914px;">
    <div class="map">
        <div id='gmap_canvas' style='height:100%;width:100%;'>

        </div>
        <script type='application/javascript'>

            var allMarkers = [];

            function initMap() {
                var myOptions = {
                    zoom: 7,
                    navigationControl: true,

                    center: new google.maps.LatLng(47.3775499, 8.4666756),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var bounds  = new google.maps.LatLngBounds();

                var map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);

                var locations = [
                        @foreach ($Profiles as $key => $Profile)
                            ["{{ $Profile->name }}", {{ $Profile->lat }}, {{ $Profile->lng }}, {{ $key }}, '/images/pin.png'],
                        @endforeach
                        ["user", {{ $_COOKIE['lat'] or '' }}, {{ $_COOKIE['lng'] or '' }}, 6, '/images/pin_red.png'],
                ];

                var infoWindow = new google.maps.InfoWindow();
                var marker, i;


                for (i = 0; i < locations.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2], locations[i][4]),
                        map: map,
                        id: locations[i][3],
                        icon: locations[i][4]
                    });

                    loc = new google.maps.LatLng(marker.position.lat(),marker.position.lng());
                    bounds.extend(loc);

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infoWindow.setContent(locations[i][0]);
                            infoWindow.open(map, marker);
                        }
                    })(marker, i));

                    allMarkers.push(marker); //Add it to allMarkers
                }
            }
            function hover(id) {
                for ( var i = 0; i< allMarkers.length; i++) {
                    if (id === allMarkers[i].id) {
                       allMarkers[i].setIcon('/images/pin_red.png');
                       break;
                    }
               }
            }
            function out(id) {
                for ( var i = 0; i< allMarkers.length; i++) {
                    if (id === allMarkers[i].id) {
                       allMarkers[i].setIcon('/images/pin.png');
                       break;
                    }
               }
            }


        </script>

    </div>
</div>