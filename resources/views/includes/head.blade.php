<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta property="og:title" content="Artikel" />
<meta property="og:site_name" content="https://familiando.ch/" />
<meta property="og:image" content="https://familiando.ch/assets/templates/base/img/familiando-logo-sui.png" />
<meta property="og:url" content="https://familiando.ch//" />
<meta name="twitter:title" content="Artikel'" />
<meta name="twitter:domain" content="https://familiando.ch/" />
<meta name="twitter:image" content="https://familiando.ch/assets/templates/base/img/familiando-logo-sui.png" />
<meta name="twitter:description" content="Lesen Sie wöchentlich neue Artikel rund um die Themen Familie, Alltag und Gesundheit. " />
<meta name="twitter:url" content="https://familiando.ch//" />

<title> @lang('gui.head.title') | @if(isset($profile)) {{ $profile->firstname . ' ' . $profile->lastname }} @else @yield('title') @endif</title>

<!-- Fonts integration -->
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300italic,300,400italic,700,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href="{{ asset('js/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>

<link href="{{ asset('css/pe-icon-7-stroke.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/pe-icon-7-stroke-helper.css') }}" rel="stylesheet" type="text/css"/>

<!-- CSS import -->
<link href="{{ asset('js/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/rundumsgeld.css') }}" rel="stylesheet">
<link href="{{ asset('css/desktop.css') }}" rel="stylesheet">
<link href="{{ asset('css/screens.css') }}" rel="stylesheet">
<link href="{{ asset('css/mobile.css') }}" rel="stylesheet">

<!-- Favicon import -->
<link href="{{ asset('/favicon.png') }}" rel="icon" />

<!-- JS import -->
<script src="{{ asset('js/default/jquery.min.js') }}"></script>
<script src="{{ asset('js/default/jquery.slidereveal.min.js') }}"></script>
<script src="{{ asset('js/default/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/default/smooth.js') }}"></script>
<script src="{{ asset('js/default/mobile.js') }}"></script>
<script src="{{ asset('js/default/viewport-units-buggyfill.js') }}"></script>
<script src="{{ asset('js/default/viewport-units-buggyfill.hacks.js') }}"></script>
<script src="{{ asset('js/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jquery-autocomplete/src/jquery.autocomplete.js') }}"></script>
@if(\App::environment('production'))
<script src="{{ asset('js/ga.js') }}"></script>
@endif

@if (in_array($viewName,['pages.user']))
<!-- Angular import-->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<script src="{{ asset('js/ng-img-crop.js') }}"></script>
<script src="{{ asset('js/angular-picture-app.js') }}"></script>
<script>
    angular.module("app").constant("CSRF_TOKEN", '{{ csrf_token() }}');
</script>
@endif