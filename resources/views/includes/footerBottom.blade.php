<div class="footer-bottom">
    <div class="container">
        <div class="col-xs-12">
            <div class="copy">
                <a href="#" class="logo"><img src="{!! asset('images/logo-footer.png') !!}" alt="Rundumsgeld" title="Rundumsgeld" /></a>
                <p>@lang('gui.footerBottom.copyright')</p>
            </div>
            <nav class="foot-bottom-menu">
                <ul>
                    <li><a href="/ueber-uns">@lang('gui.footerBottom.aboutUs')</a></li>
                    <li><a href="/kontakt">@lang('gui.footerBottom.contact')</a></li>
                    <li><a href="/impressum">@lang('gui.footerBottom.impressum')</a></li>
                    @if (Auth::guest())
                        <li><a href="/register">@lang('gui.footerBottom.register')</a></li>
                        <li><a href="/login">@lang('gui.footerBottom.login')</a></li>
                    @else
                        <li><a href="/logout">@lang('gui.footerBottom.logout')</a></li>
                    @endif
                </ul>
            </nav>
            <div class="social">
                <ul>
                    <li><a href="https://www.facebook.com/rundumsgeld" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/inforundumsgeld" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://plus.google.com/u/0/b/114268274482889421353/114268274482889421353" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="https://www.linkedin.com/company/rund-um%27s-geld" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Google api key import -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHA3vljYYGTf4pFUBUu5JwQ3T-5i4qKv4&callback=initMap" async defer></script>

