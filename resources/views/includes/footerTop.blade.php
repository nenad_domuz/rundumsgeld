<div class="footer-top">
    <div class="container">
        <div class="col-xs-12 col-sm-4 col-lg-2 foot-col">
            <h2>@lang('gui.footerTop.search.title.cities')</h2>
            <ul>
                <li><a href="/stadt/zürich">@lang('gui.footerTop.search.cities.zurich')</a></li>
                <li><a href="/stadt/geneve">@lang('gui.footerTop.search.cities.genf')</a></li>
                <li><a href="/stadt/basel">@lang('gui.footerTop.search.cities.basel')</a></li>
                <li><a href="/stadt/lausanne">@lang('gui.footerTop.search.cities.lausanne')</a></li>
                <li><a href="/stadt/bern">@lang('gui.footerTop.search.cities.bern')</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-4 col-lg-4 foot-col">
            <h2>@lang('gui.footerTop.search.title.services')</h2>
            <ul class="products-custom">
                @foreach(\App\Helpers\DataBaseHelper::GetProductOverviewList(0, 11) as $product)
                    <li>
                        <a href="/service/{{ \App\Helpers\StringHelper::transformUrl($product->name) }}/{{$product->id}}">{{$product->name}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
        {{--<div class="col-xs-12 col-sm-4 col-lg-2 foot-col">--}}
                    {{--<h2>Dienstleistungen</h2>--}}
                    {{--<ul>--}}
                    {{--@foreach(\App\Helpers\DataBaseHelper::GetProductOverviewList(1, 5) as $product)--}}
                        {{--<li><a href="/service/{{ \App\Helpers\StringHelper::transformUrl($product->name) }}/ {{$product->id}}">{{$product->name}}</a></li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                {{--</div>--}}
        <div class="col-xs-12 col-sm-4 col-lg-2 foot-col">
            <h2>@lang('gui.footerTop.search.title.companies')</h2>
            <ul>
                <li><a href="/unternehmen/axa-versicherungen">AXA Versicherungen AG</a></li>
                <li><a href="/unternehmen/helvetia-versicherungen-schweiz">Helvetia Versicherungen</a></li>
                <li><a href="/unternehmen/schweizerische-mobiliar-versicherungsgesellschaft-ag">Schweizerische Mobiliar</a></li>
                <li><a href="/unternehmen/zuerich-versicherungs-gesellschaft-ag">Zürich Versicherungen</a></li>
                <li><a href="/unternehmen/allianz-versicherungen">Allianz Versicherungen</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-4 col-lg-2 foot-col">
            <h2>@lang('gui.footerTop.search.title.distance')</h2>
            <ul>
                <li><a href="/distanz/1">@lang('gui.footerTop.distance', ['distance' => 1])</a></li>
                <li><a href="/distanz/5">@lang('gui.footerTop.distance', ['distance' => 5])</a></li>
                <li><a href="/distanz/10">@lang('gui.footerTop.distance', ['distance' => 10])</a></li>
                <li><a href="/distanz/20">@lang('gui.footerTop.distance', ['distance' => 20])</a></li>
                <li><a href="/distanz/50">@lang('gui.footerTop.distance', ['distance' => 50])</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-4 col-lg-2 foot-col">
            <h2>@lang('gui.footerTop.about.title')</h2>
            <ul>
                <li><a href="/ueber-uns">@lang('gui.footerTop.aboutUs.more')</a></li>
                <li><a href="/kontakt">@lang('gui.footerTop.aboutUs.contact')</a></li>
                {{--<li><a href="/datenschutzrichtlinie">Datenschutzrichtlinie</a></li>--}}
                {{--<li><a href="/agb">AGB</a></li>--}}
                <li>
                </li>
            </ul>
            <div class="fb-like" data-share="true" data-width="450" data-show-faces="true"></div>
        </div>
    </div>
</div>