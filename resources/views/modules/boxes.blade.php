
        <div class="negative-10 home-boxes">
            <div class="col-xs-12 col-sm-6 col-md-3 single-box">
                <div class="home-box red-box">
                    <a href="/versicherungsberatung" class="link-box">
                        <span class="image"><img src="{!! asset('images/home-box-1.jpg') !!}" alt="Insurance consulting" ></span>
                        <span class="title">Versicherungsberatung</span>
                    </a>
                    <p class="description">Wie ein massgeschneiderter Anzug nur von einem Spezialisten angefertigt werden kann, so kann auch nur ein Speziallist Ihre Versicherung auf Ihre Bedürfnisse massschneidern. Finden Sie hier Ihren Spezialisten in der Versicherungsbranche .</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 single-box">
                <div class="home-box green-box">
                    <a href="/finanzplanung" class="link-box">
                        <span class="image"><img src="{!! asset('images/home-box-2.jpg') !!}" alt="Financial planning" ></span>
                        <span class="title">Finanzplanung</span>
                    </a>
                    <p class="description">Finanzplanung ist in der heutigen Zeit ein sehr breiter und teilweise unübersichtlicher Begriff. Sie können auf unserer Plattform den für Sie geeigneten Berater auch in Finanzfragen finden. Um mehr über Finanzplanung zu erfahren klicken Sie hier.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 single-box">
                <div class="home-box brown-box">
                    <a href="/kreditberatung" class="link-box">
                        <span class="image"><img src="{!! asset('images/home-box-3.jpg') !!}" alt="Register for easier use" ></span>
                        <span class="title">Kreditberatung</span>
                    </a>
                    <p class="description">Bei einem Kreditwunsch müssen Sie Angebote vergleichen, stets Zinsen und Ihre Lage im Überblick haben. Die Übersicht zu behalten, kann daher eine heikle Angelegenheit sein. Für eine professionelle Beratung im Kreditgeschäft, klicken Sie hier. </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 single-box">
                <div class="home-box purple-box">
                    <a href="/blog" class="link-box">
                        <span class="image"><img src="{!! asset('images/home-box-4.jpg') !!}" alt="Personal benefits" ></span>
                        <span class="title">Finanz-Blog</span>
                    </a>
                    <p class="description">Jeder der bei uns angemeldet ist, bekommt die Möglichkeit seine eigenen Beiträge mit anderen zu teilen. Hinzu kommt, dass jeder Beitrag kommentiert werden kann und dadurch die Interaktion untereinander gefördert wird.</p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="col-xs-hidden col-sm-1 col-lg-2"></div>
        <div class="col-xs-12 col-sm-10 col-lg-8 text-center">
            <h2>Rund ums Geld ist die innovative Suchmaschine für Berater. Bei uns werden Sie fündig egal ob, Versicherungsberater, Finanzplaner, Treuhänder, Vermögensverwalter, Wirtschaftsanwalt.</h2>
            <p>Auf unserer Seite finden Sie alle Versicherungsberater und Finanzplaner der Schweiz. Beratung 2.0 für Ihre individuellen Bedürfnisse.</p>
        </div>
        <div class="col-xs-hidden col-sm-1 col-lg-2"></div>
