@extends('layouts.default')
@section('title')
    {{ auth()->user()->firstname . PHP_EOL . auth()->user()->lastname }}
@endsection

@section('content')
    <div>
        @include('errors.errorMessages')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Posts waiting for approval</h3>
                <span >Joined on: {{ auth()->user()->created_at->format('d.m.Y, G:H') }}</span>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>@lang('blog.admin.posts.all.table.head.postTitle')</th>
                        <th>@lang('blog.admin.posts.all.table.head.postUserName')</th>
                        <th>@lang('blog.admin.posts.all.table.head.postCreatedAt')</th>
                        <th>@lang('blog.admin.posts.all.table.head.postActions')</th>
                    </tr>
                    </thead>
                    @forelse($waitingPosts as $waitingPost)
                        <tbody>
                        <tr>
                            <td>
                                <a target="_blank" class="btn-link" href="{{ url("/blog/" . $waitingPost->slug) }}">
                                    {!! str_limit($waitingPost->title, 35) !!}
                                </a>
                            </td>
                            <td>
                                @if(isset($waitingPost->author->profile))
                                    <a target="_blank" href="{{ \App\Helpers\StringHelper::createProfilePageUrl($waitingPost->author->profile->branches[0]->company->name, $waitingPost->author->lastname, $waitingPost->author->firstname,  $waitingPost->author->profile->id) }}">
                                        {{ $waitingPost->author->profile->firstname }}&nbsp;{{ $waitingPost->author->profile->lastname }}
                                    </a>
                                @else
                                    {{ $waitingPost->author->firstname }}&nbsp;{{ $waitingPost->author->lastname }}
                                @endif
                            </td>
                            <td>
                                {{ $waitingPost->created_at->format('d.m.Y, G:H') }}
                            </td>
                            <td>
                                <form method="post" action="{{ url('/admin/approve/'.$waitingPost->id) }}">
                                    {{ csrf_field() }}
                                    <input class="btn-link" type="submit" value="@lang('blog.admin.posts.all.actions.approve')">
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td>@lang('blog.admin.posts.all.noPosts')</td>
                            </tr>
                        @endforelse
                        </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection