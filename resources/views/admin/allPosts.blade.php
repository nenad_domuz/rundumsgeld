@extends('layouts.default')
@section('title')
    {{ auth()->user()->firstname . PHP_EOL . auth()->user()->lastname }}
@endsection

@section('content')
    <div>
        @include('errors.errorMessages')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>@lang('blog.admin.posts.all.title')</h3>
                <span >@lang('blog.admin.since'){{ auth()->user()->created_at->format('M d, Y \a\t h:i a') }}</span>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th width="30%">@lang('blog.admin.posts.all.table.head.postTitle')</th>
                            <th width="20%">@lang('blog.admin.posts.all.table.head.postUserName')</th>
                            <th width="20%">@lang('blog.admin.posts.all.table.head.postCreatedAt')</th>
                            <th>@lang('blog.admin.posts.all.table.head.postActions')</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    @forelse($posts as $post)
                        <tbody>
                            <tr @if($post->deleted_at != null){!! "class='warning'" !!} @endif>
                                <td>
                                   <a target="_blank" class="btn-link" href="{{ url("/blog/" . $post->slug) }}">
                                       {!! str_limit($post->title, 35) !!}
                                   </a>
                                </td>
                                <td>
                                    @if(isset($post->author->profile))
                                        <a target="_blank" href="{{ \App\Helpers\StringHelper::CreateProfileLink($post->author->profile->branches[0]->company->name, $post->author->lastname, $post->author->firstname,  $post->author->profile->id) }}">
                                            {{ $post->author->profile->firstname }}&nbsp;{{ $post->author->profile->lastname }}
                                        </a>
                                    @else
                                        {{ $post->author->firstname }}&nbsp;{{ $post->author->lastname }}
                                    @endif
                                </td>
                                <td>
                                    {{ $post->created_at->format('d.m.Y, G:H') }}
                                </td>
                                @if($post->deleted_at == null)
                                    <td>
                                        <form method="post" action="{{ url('/admin/softDelete/'.$post->id) }}">
                                            {{ csrf_field() }}
                                            <input class="btn-link" type="submit" value="@lang('blog.admin.posts.all.actions.softDelete')">
                                        </form>
                                    </td>
                                @endif
                                @if($post->deleted_at != null)
                                    <td>
                                        <form style="float: left" method="post" action="{{ url('/admin/restore/'.$post->id) }}">
                                            {{ csrf_field() }}
                                            <input class="btn-link" type="submit" value="@lang('blog.admin.posts.all.actions.restore')">
                                        </form>
                                    </td>
                                @endif
                                <td>
                                    <form style="float: left" method="post" action="{{ url('/admin/force/'.$post->id) }}">
                                        {{ csrf_field() }}
                                        <input class="btn-link" type="submit" value="@lang('blog.admin.posts.all.actions.forceDelete')">
                                    </form>
                                </td>
                                <td>
                                    <a target="_blank" class="btn-link" href="{{ url('/blog/edit/'.$post->slug) }}">@lang('blog.admin.posts.all.actions.edit')</a>
                                </td>
                            </tr>
                    @empty
                            <tr>
                                <th>@lang('blog.admin.posts.all.noPosts')</th>
                            </tr>
                    @endforelse
                        </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection