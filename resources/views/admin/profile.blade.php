@extends('layouts.default')
@section('title')
{{ auth()->user()->firstname . PHP_EOL . auth()->user()->lastname }}
@endsection

@section('content')
<div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>@lang('blog.admin.profile.posts.latest')</h3>
            <span >@lang('blog.admin.since'){{ auth()->user()->created_at->format('d.m.Y, G:H') }}</span>
        </div>
            <div class="panel-body">
                <table class="table-padding">
                    <tr>
                        <td>@lang('blog.admin.profile.posts.total')</td>
                        <td> {{ $postsCount }}</td>
                        @if( $postsCount  > 0 )
                            <td>
                                <a href="{{ url('admin/posts/all')}}">@lang('blog.admin.profile.posts.link.showAll')</a>
                            </td>
                        @endif
                    </tr>
                    <tr>
                        <td>@lang('blog.admin.profile.posts.published')</td>
                        <td>{{ $postsPublished }}</td>
                        @if( $postsPublished > 0 )
                            <td>
                                <a href="{{ url('/admin/posts/published')}}">@lang('blog.admin.profile.posts.link.showAll')</a>
                            </td>
                        @endif
                    </tr>
                    <tr>
                        <td>@lang('blog.admin.profile.posts.waiting')</td>
                        <td>{{ $postsForApproval }}</td>
                        @if( $postsForApproval > 0 )
                            <td>
                                <a href="{{ url('/admin/posts/waiting')}}">@lang('blog.admin.profile.posts.link.showAll')</a>
                            </td>
                        @endif
                    </tr>
                    <tr>
                        <td>@lang('blog.admin.profile.posts.comments.count')</td>
                        <td>{{ $commentsCount }}</td>
                    </tr>
                </table>
            </div>
        </div>
</div>
@endsection