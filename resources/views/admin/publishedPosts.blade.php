@extends('layouts.default')
@section('title')
    {{ auth()->user()->firstname . PHP_EOL . auth()->user()->lastname }}
@endsection

@section('content')
    <div>
        @include('errors.errorMessages')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>@lang('blog.admin.posts.published.title')</h3>
                <span >@lang('blog.admin.since'){{ auth()->user()->created_at->format('d.m.Y, G:H') }}</span>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>@lang('blog.admin.posts.all.table.head.postTitle')</th>
                            <th>@lang('blog.admin.posts.all.table.head.postUserName')</th>
                            <th>@lang('blog.admin.posts.all.table.head.postCreatedAt')</th>
                        </tr>
                    </thead>
                    @forelse($publishedPosts as $publishedPost)
                        <tbody>
                            <tr>
                                <td>
                                    <a target="_blank" class="btn-link" href="{{ url("/blog/" . $publishedPost->slug) }}">
                                        {!! str_limit($publishedPost->title, 35) !!}
                                    </a>
                                </td>
                                <td>
                                    @if(isset($publishedPost->author->profile))
                                        <a target="_blank" href="{{ \App\Helpers\StringHelper::createProfilePageUrl($publishedPost->author->profile->branches[0]->company->name, $publishedPost->author->lastname, $publishedPost->author->firstname,  $publishedPost->author->profile->id) }}">
                                            {{ $publishedPost->author->profile->firstname }}&nbsp;{{ $publishedPost->author->profile->lastname }}
                                        </a>
                                    @else
                                        {{ $publishedPost->author->firstname }}&nbsp;{{ $publishedPost->author->lastname }}
                                    @endif
                                </td>
                                <td>
                                    {{ $publishedPost->created_at->format('d.m.Y, G:H') }}
                                </td>
                            </tr>
                    @empty
                            <tr>
                                <th>@lang('blog.admin.posts.all.noPosts')</th>
                            </tr>
                    @endforelse
                        </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection