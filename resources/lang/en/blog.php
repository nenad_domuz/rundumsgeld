<?php

return [
    'noPosts.title' => 'There are not posts yet. Be the first who writes a post.',
    'posts.page' => 'Blog posts',
    'posts.yourPosts' => 'Your blog posts',
    'nav.editPost' => 'Edit Post',
    'nav.editDraft' => 'Edit Draft',
    'post.by' => 'by',
    'blog.page.notExists' => 'Page does not exist',
    'post.create.title.placeholder' => 'Enter title here',

    'post.button.publish' => 'Publish',
    'post.button.saveDraft' => 'Save as draft',
    'post.button.update' => 'Update',
    'post.button.delete' => 'Delete',

    'post.leaveComment' => 'Leave a comment',
    'post.loginToComment' => 'Login to Comment',
    'post.enterComment' => 'Enter comment here',
    'post.postComment' => 'Post',

    'post.label.approved' => 'Approved',
    'post.label.waiting' => 'Waiting for approval',

    'admin.since' => 'Admin since: ',

    'admin.profile.posts.latest' => 'Latest posts',
    'admin.profile.posts.total' => 'Total posts',
    'admin.profile.posts.published' => 'Published posts',
    'admin.profile.posts.waiting' => 'Posts waiting for approval',
    'admin.profile.posts.comments.count' => 'Total comments',
    'admin.profile.posts.link.showAll' => 'Show All',

    'admin.posts.all.title' => 'All posts',
    'admin.posts.all.noPosts' => 'No posts',
    'admin.posts.all.table.head.postTitle' => 'Title',
    'admin.posts.all.table.head.postUserName' => 'User name',
    'admin.posts.all.table.head.postCreatedAt' => 'Created at',
    'admin.posts.all.table.head.postActions' => 'Actions',

    'admin.posts.all.actions.softDelete' => 'Soft delete',
    'admin.posts.all.actions.forceDelete' => 'Force delete',
    'admin.posts.all.actions.restore' => 'Restore',
    'admin.posts.all.actions.edit' => 'Edit',
    'admin.posts.all.actions.approve' => 'Approve',

    'admin.posts.published.title' => 'Published posts',

];