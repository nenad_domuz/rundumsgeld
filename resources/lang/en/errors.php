<?php
    return [
        'title.paragraph1' => 'Ooops... Something went wrong',
        'title.paragraph2' => 'You can go back to',
        'title.paragraph.home' => 'Home',
        'title.paragraph2.2' => 'if you wish',
        'title.paragraph3' => 'or use some of the helpful links instead',

        'title.appDown' => 'We are currently offline.',

        'menu.1' => 'Versicherungsberatung',
        'menu.2' => 'Finanzplanung',
        'menu.3' => 'Kreditberatung',
        'menu.4' => 'Finanz-Blog',
        'menu.5' => 'Ihr Profil',

        'notAllowed' => 'will be back...',

        'user.profile.button.update' => 'Update successful',
    ];