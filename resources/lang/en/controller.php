<?php

return [

    'blogComment.published' => 'Comment published',

    'blogPost.title' => 'Newest posts',
    'blogPost.permission' => 'You do not have sufficient permissions to do that.',
    'blogPost.titleExists' => 'Title already exists',
    'blogPost.saved' => 'Post saved successfully',
    'blogPost.published' => 'Post published successfully',
    'blogPost.requestNotFound' => 'Requested post not found',
    'blogPost.updated' => 'Post updated successfully',
    'blogPost.deleted' => 'Post deleted successfully',


    'contact.subject' => 'It\'s from the contact form',
    'contact.thankYou' => 'Thank you for your message',
    'contact.contacted' => 'Agent was contacted',

    'auth.activated' => 'You have successfully activated your account.',
    'auth.mailSent' => 'We\'ve sent you an activation code. Please check your email.',

];
