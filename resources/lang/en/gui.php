<?php

return [

    'head.title' => 'Rund ums Geld',
    'home.title' => 'Home',

    'footerTop.distance' => 'Within :distance km',

    'footerTop.search.title.cities' => 'Cities',
    'footerTop.search.title.services' => 'Services',
    'footerTop.search.title.companies' => 'Companies',
    'footerTop.search.title.distance' => 'Distance',
    'footerTop.about.title' => 'About us',

    'footerTop.aboutUs.more' => 'More about us',
    'footerTop.aboutUs.contact' => 'Contact',

    'footerTop.search.cities.zurich' => 'Consultant in Zürich',
    'footerTop.search.cities.genf' => 'Consultant in Genf',
    'footerTop.search.cities.basel' => 'Consultant in Basel',
    'footerTop.search.cities.lausanne' => 'Consultant in Lausanne',
    'footerTop.search.cities.bern' => 'Consultant in Bern',

    'footerBottom.copyright' => 'Copyright 2016 by Rundumsgeld. All rights reserved.',
    'footerBottom.aboutUs' => 'About us',
    'footerBottom.contact' => 'Contact',
    'footerBottom.impressum' => 'Impressum',
    'footerBottom.register' => 'Register',
    'footerBottom.login' => 'Login',
    'footerBottom.logout' => 'Logout',

    'menu.yourProfile' => 'Your profile',
    'menu.financeBlog' => 'Finance Blog',
    'menu.myPosts' => 'My Posts',
    'menu.addPost' => 'New post',
    'menu.logout' => 'Logout',

    'home.searchCity.placeholder' => 'Enter a city\'s name...',
    'searchNoResults' => 'No Results... Maybe try something else?',

    'search' => 'Search',
    'search.distance' => 'Distance',
    'search.viewOnMap' => 'View on map',
    'search.titlePrefix' => 'Search by',

    'contact.name' => 'Your name',
    'contact.name.placeholder' => 'Enter your name.',
    'contact.email' => 'E-mail address',
    'contact.email.placeholder' => 'Enter your e-mail address.',
    'contact.website' => 'Your website',
    'contact.website.placeholder' => 'Enter your website\'s address.',
    'contact.message' => 'Message',
    'contact.message.placeholder' => 'Enter your message.',
    'contact.send' => 'Send message',
    'contact.reset' => 'Reset form',

    'profile.additionalInformation.none' => 'We don\'t know which products this consultant offers.',

    'login' => 'Login',
    'login.password' => 'Password',
    'login.rememberMe' => 'Remember Me',
    'login.forgotPassword' => 'Forgot password',

    'resetPassword' => 'Reset password',
    'sendPasswordLink' => 'Send link for password',

    'register' => 'Register',
    'register.firstname' => 'First name',
    'register.firstname.placeholder' => 'Enter your first name.',
    'register.lastname' => 'Last name',
    'register.lastname.placeholder' => 'Enter your first name.',
    'register.finmanumber' => 'FINMA number',
    'register.finma.placeholder' => 'Enter your FINMA registration number.',
    'register.confirmPassword' => 'Confirm password',
    'register.notAllowed' => 'Not Allowed!',
    'register.notAllowed.text' => 'We cannot find your email or finma number related to your name in our database. Please contact us using the ',
    'register.notAllowed.button' => 'Contact form',

    'userActivated' => 'Willkommen bei Rund ums Geld',
    'userActivated.goToProfile' => 'Go to your profile',

    'modal.title' => 'When do you want to book this agent?',
    'modal.header' => 'Please fill in all required information',
    'modal.buttonSend' => 'Send',
    'modal.buttonContact' => 'Contact this agent',
    'modal.buttonClose' => 'Close',

    'profile.aboutMe' => 'About me',
    'profile.aboutMe.noInfo' => 'This agent has not provided his information for the moment',
    'profile.skills' => 'Skills',
    'profile.education' => 'Education',
    'profile.hobbies' => 'Hobbies',
    'profile.training' => 'Training',
    'profile.products' => 'Products',

    'profile.aside.distance' => 'Distance',
    'profile.aside.city' => 'City',
    'profile.aside.dateOfBirth' => 'Date of birth',
    'profile.aside.languages' => 'Languages',
    'profile.aside.businessContact' => 'Business contact',
    'profile.aside.businessAddress' => 'Business address',
    'profile.aside.privateAddress' => 'Private address',

    'user.profileInformation' => 'Profile information',
    'user.phone.title' => 'Phone',
    'user.phone.appearsAs' => '*appears like this: +41 (12) 34 567 89',
    'user.fax.title' => 'Fax',
    'user.fax.appearsAs' => '*appears like this: +41 (98) 76 543 21',
    'user.mobile.title' => 'Mobile',
    'user.finma.title' => 'Your FINMA number',
    'user.finma.cannotChange' => '*cannot be changed',
    'user.button.updateImage' => 'Update image',
    'user.button.updateProfile' => 'Update profile',
    'user.button.viewProfile' => 'View Profile',
    'user.profilePicture' => 'Profile picture',

    'picture.upload' => 'Update image',

    'blog.readMore' => 'Read more',

    'blog.create' => 'Create Post',

    'blog.edit' => 'Edit Post',

];
