<?php

return [

    'userAction.searchConsultant' => 'You\'re a customer? Search for a consultant.',
    'userAction.registerConsultant' => 'You\'re a consultant? Register to update your profile.',

];
