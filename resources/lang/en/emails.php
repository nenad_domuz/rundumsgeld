<?php

return [

    'activation.title'   => 'Click <a style="color:#3f65b1;" href=":link">here</a> to activate your account.',
    'activation.text' => 'Hi <span style="color: #3f65b1">:firstname :lastname</span><span style="display: none;">:finmaNumber</span>. Based on your request, we\'ve sent you this activation email with a corresponding link.<br/> Your FINMA-Number: :finmaNumber\'',
    'password.title' => 'Here is your password reset link',
    'password.text' => 'Click here to reset your password: <a href=":link">:link</a>',

    'profile.update.email.subject' => 'A User updated his profile',

    'contact.agent.email.title' => 'Agent was contacted',

    'blog.newPost.title' => 'New post is waiting for approval',
];
