<?php

return [

    'activation.mailSender' => 'Rund ums Geld.',
    'activation.mailTitle' => 'Activation of your account at Rund ums Geld.',
    'activation.asvmMail.activationLog' => 'Activation Log!',

    'asvmMail.recipient' => 'ASVM-Info',

];

