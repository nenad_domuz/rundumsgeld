<?php

return [

    'userAction.searchConsultant' => 'Sie sind ein Kunde? Suchen Sie Ihren Berater.',
    'userAction.registerConsultant' => 'Sie sind ein Berater? Registrieren Sie sich, um Ihr Profil zu aktualisieren.',

];
