<?php

return [

    'activation.title'   => 'Klicken Sie <a style="color:#3f65b1;" href=":link">hier</a>, um Ihren Zugang zu aktivieren.',
    'activation.text' => 'Hallo <span style="color: #3f65b1">:firstname :lastname</span>. Aufgrund Ihrer Anfragen haben wir Ihnen diese Aktivierungsemail mit dem entsprechenden Link zugestellt.<br/> Ihre FINMA-Nummer: :finmaNumber',
    'password.title' => 'Link zum Zurücksetzen Ihres Passwortes',
    'password.text' => 'Klicken Sie hier, um Ihr Passwort zurück zu setzen: <a href=":link">:link</a>',

    'profile.update.email.subject' => 'Ein Benutzer hat sein Profil aktualisiert.',

    'contact.agent.email.title' => 'Ein Berater wurde kontaktiert.'

];
