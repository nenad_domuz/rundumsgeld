<?php

return [

    'activation.mailSender' => 'Rund ums Geld.',
    'activation.mailTitle' => 'Aktivieren Sie Ihren Account bei Rund ums Geld.',
    'activation.asvmMail.activationLog' => 'Aktivierung!',

    'asvmMail.recipient' => 'ASVM-Info',

];

