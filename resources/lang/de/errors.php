<?php
    return [
        'title.paragraph1' => 'Uuupps... Da ist etwas schief gelaufen ',
        'title.paragraph2' => 'Gehen Sie zurück zur',
        'title.paragraph.home' => 'Homepage',
        'title.paragraph2.2' => 'wenn Sie möchten',
        'title.paragraph3' => 'oder benutzen Sie einen der folgenden Links',

        'title.appDown' => 'Wir sind gleich zurück.',

        'menu.1' => 'Versicherungsberatung',
        'menu.2' => 'Finanzplanung',
        'menu.3' => 'Kreditberatung',
        'menu.4' => 'Finanz-Blog',
        'menu.5' => 'Ihr Profil',

        'notAllowed' => 'ist gleich zurück...',

        'user.profile.button.update' => 'Profil erfolgreich aktualisiert',
    ];