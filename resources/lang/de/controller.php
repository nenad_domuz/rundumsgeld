<?php

return [

    'blogComment.published' => 'Kommentar veröffentlicht',

    'blogPost.title' => 'Neuste Beiträge',
    'blogPost.permission' => 'Sie haben nicht die notwendige Berechtigung dazu.',
    'blogPost.titleExists' => 'Title existiert bereits',
    'blogPost.saved' => 'Post erfolgreich gespeichert',
    'blogPost.published' => 'Post erfolgreich veröffentlicht',
    'blogPost.requestNotFound' => 'Gesuchter Post nicht gefunden',
    'blogPost.updated' => 'Post erfolgreich aktualisiert',
    'blogPost.deleted' => 'Post erfolgreich gelöscht',


    'contact.subject' => 'Diese Nachricht stammt vom Kontaktformular',
    'contact.thankYou' => 'Vielen Dank für Ihre Mitteilung.',
    'contact.contacted' => 'Der Berater wurde kontaktiert.',

    'auth.activated' => 'Sie haben Ihren Zugang erfolgreich aktiviert.',
    'auth.mailSent' => 'Wir haben Ihnen einen Aktivierungscode zugestellt. Bitte überprüfen Sie Ihre Emails.',
];
