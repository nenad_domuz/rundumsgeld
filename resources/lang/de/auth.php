<?php

return [

    'failed'   => 'Diese Zugangsdaten wurden nicht in unserer Datenbank gefunden.',
    'throttle' => 'Zu viele Login Versuche. Versuchen Sie es bitte in :seconds Sekunden.',

];