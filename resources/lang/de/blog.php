﻿<?php

return [
    'noPosts.title' => 'Es gibt noch keine Posts. Seien Sie der erste, der einen Artikel verfasst.',
    'posts.page' => ' Finanz-Blog ',
    'posts.yourPosts' => 'Ihre Posts',
    'nav.editPost' => 'Post ändern',
    'nav.editDraft' => 'Entwurf ändern',
    'post.by' => 'von',
    'blog.page.notExists' => 'Seite existiert nicht.',
    'post.create.title.placeholder' => 'Bitte Titel hier eingeben.',

    'post.button.publish' => 'Veröffentlichen',
    'post.button.saveDraft' => 'Entwurf speichern',
    'post.button.update' => 'Aktualisieren',
    'post.button.delete' => 'Löschen',

    'post.leaveComment' => 'Kommentar hinterlassen',
    'post.loginToComment' => 'Zum Kommentieren anmelden',
    'post.enterComment' => 'Kommentar hier eingeben',
    'post.postComment' => 'Post',

    'post.label.approved' => 'Approved',
    'post.label.waiting' => 'Waiting for approval',

    'admin.since' => 'Admin seit: ',

    'admin.profile.posts.latest' => 'Neuester Post',
    'admin.profile.posts.total' => 'Total Posts',
    'admin.profile.posts.published' => 'Veröffentlichte Posts',
    'admin.profile.posts.waiting' => 'Auf Zustimmung wartende Posts ',
    'admin.profile.posts.comments.count' => 'Total Kommentare',
    'admin.profile.posts.link.showAll' => 'Alle anzeigen',

    'admin.posts.all.title' => 'Alle Posts',
    'admin.posts.all.noPosts' => 'Keine Posts',
    'admin.posts.all.table.head.postTitle' => 'Titel',
    'admin.posts.all.table.head.postUserName' => 'Benutzername',
    'admin.posts.all.table.head.postCreatedAt' => 'Erstellt am',
    'admin.posts.all.table.head.postActions' => 'Aktionen',

    'admin.posts.all.actions.softDelete' => 'Als geslöscht markieren',
    'admin.posts.all.actions.forceDelete' => 'Löschen',
    'admin.posts.all.actions.restore' => 'Wiederherstellen',
    'admin.posts.all.actions.edit' => 'Bearbeiten',
    'admin.posts.all.actions.approve' => 'Zustimmen',

    'admin.posts.published.title' => 'Veröffentlichte Posts',
];
