<?php

return [

    'head.title' => 'Rund ums Geld',
    'home.title' => 'Startseite',

    'footerTop.distance' => 'Im Umkreis von :distance km',

    'footerTop.search.title.cities' => 'Städte',
    'footerTop.search.title.services' => 'Dienstleistungen',
    'footerTop.search.title.companies' => 'Gesellschaften',
    'footerTop.search.title.distance' => 'Entfernung',
    'footerTop.about.title' => 'Über uns',

    'footerTop.aboutUs.more' => 'Mehr über uns',
    'footerTop.aboutUs.contact' => 'Kontakt',

    'footerTop.search.cities.zurich' => 'Berater in Zürich',
    'footerTop.search.cities.genf' => 'Berater in Genf',
    'footerTop.search.cities.basel' => 'Berater in Basel',
    'footerTop.search.cities.lausanne' => 'Berater in Lausanne',
    'footerTop.search.cities.bern' => 'Berater in Bern',

    'footerBottom.copyright' => 'Copyright 2016 bei Rundumsgeld. Alle Rechte vorbehalten.',
    'footerBottom.aboutUs' => 'Über uns',
    'footerBottom.contact' => 'Kontakt',
    'footerBottom.impressum' => 'Impressum',
    'footerBottom.register' => 'Registrieren',
    'footerBottom.login' => 'Login',
    'footerBottom.logout' => 'Logout',

    'menu.yourProfile' => 'Ihr Profil',
    'menu.financeBlog' => 'Finanz-Blog',
    'menu.myPosts' => 'Ihre Posts',
    'menu.addPost' => 'Neuer Post',
    'menu.logout' => 'Logout',

    'home.searchCity.placeholder' => 'Geben Sie eine Stadt ein...',
    'searchNoResults' => 'Nichts gefunden... Vielleicht versuchen Sie es noch einmal?',

    'search' => 'Suche',
    'search.distance' => 'Distanz',
    'search.viewOnMap' => 'Auf Karte zeigen',
    'search.titlePrefix' => 'Suche nach',

    'contact.name' => 'Ihr Name',
    'contact.name.placeholder' => 'Geben Sie Ihren Namen ein.',
    'contact.email' => 'Email-Adresse',
    'contact.email.placeholder' => 'Geben Sie Ihre Email-Adresse ein.',
    'contact.website' => 'Ihre Webseite',
    'contact.website.placeholder' => 'Geben Sie die Adresse Ihrer Webseite ein.',
    'contact.message' => 'Nachricht',
    'contact.message.placeholder' => 'Geben Sie Ihre Nachricht ein.',
    'contact.send' => 'Nachricht senden',
    'contact.reset' => 'Formular zurücksetzen',

    'profile.additionalInformation.none' => 'Wir wissen nicht, welche Produkte dieser Berater anbietet.',
    
    'login' => 'Login',
    'login.password' => 'Passwort',
    'login.rememberMe' => 'Angemeldet bleiben',
    'login.forgotPassword' => 'Passwort vergessen',

    'resetPassword' => 'Passwort zurücksetzen',
    'sendPasswordLink' => 'Passwort-Link zustellen',

    'register' => 'Registrieren',
    'register.firstname' => 'Vorname',
    'register.firstname.placeholder' => 'Geben Sie Ihren Vornamen ein.',
    'register.lastname' => 'Nachname',
    'register.lastname.placeholder' => 'Geben Sie Ihren Nachnamen ein.',
    'register.finmanumber' => 'FINMA-Nummer',
    'register.finma.placeholder' => 'Geben Sie Ihre FINMA-Nummer ein.',
    'register.confirmPassword' => 'Passwort bestätigen',
    'register.notAllowed' => 'Nicht erlaubt!',
    'register.notAllowed.text' => 'Wir können in unserer Datanbank keine Email-Adresse oder eine FINMA-Nummer finden, die zu Ihrem Namen passt. Bitte kontaktieren Sie uns mit Hilfe des ',
    'register.notAllowed.button' => 'Kontakt-Formulars',

    'userActivated' => 'Willkommen bei Rund ums Geld',
    'userActivated.goToProfile' => 'Öffnen Sie Ihr Profil',

    'modal.title' => 'Wann möchten Sie mit dem Berater einen Termin vereinbaren?',
    'modal.header' => 'Geben Sie alle notwendigen Informationen ein',
    'modal.buttonSend' => 'Senden',
    'modal.buttonClose' => 'Schliessen',
    'modal.buttonContact' => 'Diesen Berater kontaktieren',

    'profile.aboutMe' => 'Über mich',
    'profile.aboutMe.noInfo' => 'Dieser Berater hat uns bis anhin noch nichts Näheres über sich erzählt.',
    'profile.skills' => 'Erfahrung',
    'profile.education' => 'Ausbildung',
    'profile.hobbies' => 'Hobbies',
    'profile.training' => 'Trainings',
    'profile.products' => 'Produkte',

    'profile.aside.distance' => 'Distanz',
    'profile.aside.city' => 'Stadt',
    'profile.aside.dateOfBirth' => 'Geburtsdatum',
    'profile.aside.languages' => 'Sprache',
    'profile.aside.businessContact' => 'Geschäftskontakt',
    'profile.aside.businessAddress' => 'Geschäftsadresse',
    'profile.aside.privateAddress' => 'Privatadresse',

    'user.profileInformation' => 'Profil-Informationen',
    'user.phone.title' => 'Telefon',
    'user.phone.appearsAs' => '*erscheint als: +41 (12) 34 567 89',
    'user.fax.title' => 'Fax',
    'user.fax.appearsAs' => '*erscheint als: +41 (98) 76 543 21',
    'user.mobile.title' => 'Mobiltelefon',
    'user.finma.title' => 'Ihre FINMA-Nummer',
    'user.finma.cannotChange' => '*kann nicht geändert werden',
    'user.button.updateImage' => 'Bild aktualisieren',
    'user.button.updateProfile' => 'Profil aktualisieren',
    'user.button.viewProfile' => 'Profile anzeigen',
    'user.profilePicture' => 'Profilbild',

    'picture.upload' => 'Bild hochladen',

    'blog.readMore' => 'Weiter lesen',

    'blog.create' => 'Post erstellen',

    'blog.edit' => 'Post bearbeiten',

];