<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Posts extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function comments()
    {
        return $this->hasMany('App\Comments','post_id');
    }

    public function author()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'user_id');
    }
}
