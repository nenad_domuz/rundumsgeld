<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Profile extends Model
{
    protected $table = 'profiles';

    public function branches()
    {
        return $this->belongsToMany('App\Branch', 'profiles_branches', 'profile_id', 'branch_id');
    }

    public function post()
    {
        return $this->belongsTo('App\Posts','user_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
