<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Enables access to View names
     *
     * @paramname string $viewName
     *
     */
    public function boot()
    {
        view()->composer('*', function($view){
            $viewName = $view->getName();
            view()->share('viewName', $viewName);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
