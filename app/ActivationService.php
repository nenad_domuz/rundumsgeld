<?php

namespace App;

use App\Helpers\StringHelper;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mail;

class ActivationService
{
    protected $mailer;

    protected $activationRepo;

    protected $resendAfter = 24;

    public function __construct(Mailer $mailer, ActivationRepository $activationRepo)
    {
        $this->mailer = $mailer;
        $this->activationRepo = $activationRepo;
    }

    public function sendActivationMail($user)
    {
        if($user->activated || !$this->shouldSend($user))
        {
            return;
        }

        $input = Request::all();

        $token = $this->activationRepo->createActivation($user);
        $link = route('user.activate', $token);

        if (!empty($input['finma_registration_number']))
        {
            $finmaNumber = $input['finma_registration_number'];
        }
        else
        {
            $finmaNumber = '';
        }

        $this->mailer->send('auth.emails.activation', [
            'user' => $user,
            'token' => $token,
            'link' => $link,
            'finmaNumber' => $finmaNumber,
            'firstname' => $input['firstname'],
            'lastname' => $input['lastname'],
            'pathToImage' => public_path()."/images/logo-header.png"
        ], function(Message $m) use ($user){
            $m->from('no-reply@rundumsgeld.ch', trans('services.activation.mailSender'));
            $m->to($user->email)->subject(trans('services.activation.mailTitle'));
        });
    }

    public function activateUser($token)
    {
        $activation = $this->activationRepo->getActivationByToken($token);

        if($activation === null) {
            return null;
        }

        $user = User::find($activation->user_id);
        $user->is_approved = true;
        $user->save();

        DB::table('log')->where('user_id', $activation->user_id)
                        ->update([
                            'is_approved' => true,
                            'updated_at' => Carbon::now(),
                        ]);

        if(!empty($activation->finma_registration_number))
        {
            DB::table('profiles')->where('finma_registration_number', $activation->finma_registration_number)
                ->update([
                    'user_id' => $activation->user_id,
                    'email' => $user->email
                ]);
        }
        else
        {
            DB::table('profiles')->where('email', $user->email)
                ->update([
                    'user_id' => $activation->user_id,
                ]);
        }

        $profile = Profile::where('user_id', $activation->user_id)->first();
        $link = 'https://www.rundumsgeld.ch' . StringHelper::transformUrl('/berater/'.$profile->branches[0]->company->name.'/'.$profile->lastname.'/'.$profile->firstname.'/'.$profile->id);

        Mail::send('auth.emails.activationLog', [
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'email' => $user->email,
            'link' => $link,
        ], function ($message) use ($user)
        {
            $message->from(env('MAIL_USERNAME'), trans('services.activation.mailSender'));
            $message->to(env('MAIL_RECIPIENT_ASVM'), 'ASVM Info')->subject(trans('services.activation.asvmMail.activationLog') . ' ' . $user->firstname . ' ' . $user->lastname);
        });

        $this->activationRepo->deleteActivation($token);

        return $user;
    }

    private function shouldSend($user)
    {
        $activation = $this->activationRepo->getActivation($user);
        return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
    }
}