<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SenderEmail extends Model
{
    protected $table = 'mails';
}
