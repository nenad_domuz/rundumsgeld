<?php

namespace App\Helpers;
use App\Helpers\Log;
class StringHelper
{
    public static function getHelloWorld()
    {
        return "Hello world!";
    }

    public static function transformUrl($string)
    {
        $transliterationTable = array('´' => '', ' ' => '-', '.' => '', ',' => '', '&' => '', 'á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'dj', 'Ḋ' => 'D', 'đ' => 'dj', 'Đ' => 'DJ', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'oe', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');
        return strtolower(str_replace(array_keys($transliterationTable), array_values($transliterationTable), strip_tags(trim($string))));
    }
    public static function createProfilePageUrl($companyName, $lastname, $firstname, $id)
    {
        return StringHelper::transformUrl('/berater/'.$companyName.'/'.$lastname.'/'.$firstname.'/'.$id);
    }
    public static function formatPhone($Phone)
    {
        if (preg_match('/[+]?41\d{9}$/', $Phone))
        {
            $formatedPhoneNumber = '+41 (' . substr($Phone, -9, 2) . ') ' . substr($Phone, -7, 3) . ' ' . substr($Phone, -4, 2) . ' ' . substr($Phone, -2, 2);
        }
        else if (preg_match('/0\d{9}$/', $Phone))
        {
            $formatedPhoneNumber = '+41 (' . substr($Phone, -9, 2) . ') ' . substr($Phone, -7, 3) . ' ' . substr($Phone, -4, 2) . ' ' . substr($Phone, -2, 2);
        }
        else if (preg_match('/[+]?(41)?423\d{7}$/', $Phone))
        {
            $formatedPhoneNumber = '+423 ' . substr($Phone, -7, 3) . ' ' . substr($Phone, -4, 2) . ' ' . substr($Phone, -2, 2);
        }

        else
        {
            $formatedPhoneNumber = trans('helper.string.notAvailable');
        }
        return $formatedPhoneNumber;
    }

    public static function companyNameFromURI($name)
    {
        $companyName = str_replace("-", " ", $name);
        if (strpos($companyName, 'zuerich') === 0) {
            $companyName = StringHelper::strCustomOccurencyReplace(" ", "-", $companyName);
        }
        return $companyName;
    }


    public static function strCustomOccurencyReplace($search, $replace, $subject)
    {
        $pos1 = strpos($subject, $search);
        $pos2 = strpos($subject, $search, $pos1 + strlen($search));

        if($pos2 !== false)
        {
            $subject = substr_replace($subject, $replace, $pos2, strlen($search));
        }

        return $subject;
    }

    public static function getTitleForSearchResult($url)
    {
        $url = urldecode($url);

        $uriSegment = explode('/', $url);

        if(isset($uriSegment[4]))
        {
            if(preg_match("/^[0-9]+$/", $uriSegment[4]) === 0)
            {
                $title = ucwords($uriSegment[4]);

                if (strpos($title, '?') !== false)
                {
                    $titleExp = explode('?', $title);
                    if(preg_match("/^[0-9]+$/", $titleExp[0]) === 1)
                    {
                        $title = $titleExp[0] . ' Km';
                    }
                    else
                    {
                        $title = $titleExp[0];

                    }
                }

            } else
            {
                $title = ucwords($uriSegment[4]) . ' Km';

            }
        }
        else
        {
            $city = explode('=', $uriSegment[3]);
            if (strpos($city[1], '?') !== false)
            {
                $titleExp = explode('?', $city[1]);
                $title = $titleExp[0];
            }
            else
            {
                $title = $city[1];
            }
        }

        $umlauts = array('ae' => 'ä', 'oe' => 'ö', 'ue' => 'ü','Ae' => 'Ä','Oe' => 'Ö', 'Ue' => 'Ü', '-' => ' ');
        $title = strtolower(str_replace(array_keys($umlauts), array_values($umlauts), strip_tags(trim($title))));

        return  ucwords($title);
    }
}