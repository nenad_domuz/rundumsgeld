<?php

namespace App\Helpers;

use App\Profile;
use App\SenderEmail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Product;
use Mail;

class DataBaseHelper
{
    public static function GetBasicProfileSearchQuery ()
    {
        return DB::table('profiles')
                    ->join('profiles_branches', 'profiles.id', '=', 'profiles_branches.profile_id')
                    ->join('branches', 'profiles_branches.branch_id', '=', 'branches.id')
                    ->join('companies', 'branches.company_id', '=', 'companies.id')
                    ->orderBy('profiles.rank', 'desc')
                    ->orderBy('profiles.id', 'asc')
                    ->select('profiles.*', 'branches.name', 'branches.latitude as lat', 'branches.longitude as lng', 'branches.city', 'companies.name as CompanyName');
    }

    public static function GetCitiesOnly()
    {
        return DB::table('branches')->select('branches.city as value')->distinct();
    }

    public static function GetBasicProfileSearchQueryProducts ()
    {
        return DB::table('profiles')
                    ->join('profiles_branches', 'profiles.id', '=', 'profiles_branches.profile_id')
                    ->join('branches', 'profiles_branches.branch_id', '=', 'branches.id')
                    ->join('profiles_products', 'profiles.id', '=', 'profiles_products.profile_id')
                    ->join('insurance_companies', 'profiles_products.insurance_company_id', '=', 'insurance_companies.id')
                    ->join('products', 'profiles_products.product_id', '=', 'products.id')
                    ->join('companies', 'branches.company_id', '=', 'companies.id')
                    ->orderBy('rank', 'desc')
                    ->orderBy('id', 'asc')
                    ->select('profiles.*', 'branches.name', 'branches.latitude as lat', 'branches.longitude as lng', 'companies.name as CompanyName', 'products.name as ProductName', 'products.id as ProductId', 'insurance_companies.name as InsuranceCompanyName');
    }

    public static function GetProductOverviewList ($page = 0, $itemCount = 5)
    {
        return Product::skip($page * $itemCount)->take($itemCount)->get();
    }

    public static function GetProductOverviewListForProfile ($id)
    {
        return DataBaseHelper::GetBasicProfileSearchQueryProducts()->where('profiles.id', $id)->groupBy('products.id')->get();
    }

    public static function GetCommentsWithUsers ($slug)
    {
        return DB::table('comments')
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->join('posts', 'comments.post_id', '=', 'posts.id')
            ->select('comments.*','users.firstname','users.lastname', 'posts.id', 'posts.slug')
            ->where('posts.slug', $slug)
            ->get();
    }

    public static function GetBranchesDistance ($id, $lat, $lng)
    {
        return DB::select('select p.id, p.firstname,
        (((acos(sin(('.$lat.' * pi() / 180)) * sin((b.latitude * pi() / 180)) + cos(('.$lat.' * pi() / 180)) * cos((b.latitude * pi() / 180)) * cos((('.$lng.' - b.longitude) * pi() / 180)))) * 180 / pi()) * 60 * 1.1515*1.609344) as distance
        from profiles p
        left join profiles_branches pb on p.id = pb.profile_id
        left join branches b on pb.branch_id = b.id
        left join profiles_products pp on p.id = pp.profile_id
        left join products pr on pp.product_id = pr.id
        left join companies c on b.company_id = c.id
        WHERE p.id = '.$id.' and b.latitude <> 0');
    }

    public static function GetSearchDistance ($distance)
    {
        if (isset($_COOKIE['lat']))
        {
            $lat = $_COOKIE['lat'];
            $lng = $_COOKIE['lng'];
            return DB::select('select *, c.name as CompanyName, p.id as id, b.latitude as lat, b.longitude as lng,
            (((acos(sin((' . $lat . ' * pi() / 180)) * sin((b.latitude * pi() / 180)) + cos((' . $lat . ' * pi() / 180)) * cos((b.latitude * pi() / 180)) * cos(((' . $lng . ' - b.longitude) * pi() / 180)))) * 180 / pi()) * 60 * 1.1515*1.609344) as distance
            from profiles p
            left join profiles_branches pb on p.id = pb.profile_id
            left join branches b on pb.branch_id = b.id
            left join profiles_products pp on p.id = pp.profile_id
            left join products pr on pp.product_id = pr.id
            left join companies c on b.company_id = c.id
            GROUP BY p.id
            HAVING distance < ' . $distance . '
            ORDER BY p.rank DESC, p.id ASC');
        }
        else
        {
            return false;
        }
    }

    public static function ViewCounterProfile ($id, $ip)
    {
        $profileUserId = Profile::select('user_id')->where('id', $id)->get();

        if (!isset(auth()->user()->id))
        {
            DB::table('views')->insert(
                ['profile_id' => $id, 'ip' => $ip, 'created_at' => Carbon::now()]
            );

        }
        elseif (isset(auth()->user()->id) && auth()->user()->id != $profileUserId[0]['user_id'])
        {
            DB::table('views')->insert(
                ['profile_id' => $id, 'ip' => $ip, 'created_at' => Carbon::now()]
            );
        }

       DataBaseHelper::UpdateProfileRank();
    }


    public static function UpdateProfileRank()
    {
        //$data = DB::select("SELECT id, viewed, case when email = '' then 0 else 1 end as email, case when image_blob is null then 0 else 1 end as image FROM profiles");
        $data = DB::select("SELECT profiles.id, profiles.viewed, count(views.id) as viewed_new ,  case when profiles.email = '' then 0 else 1 end as email, case when profiles.image_blob is null then 0 else 1 end as image FROM profiles
                            LEFT  JOIN views
                            ON profiles.id = views.profile_id
                            GROUP BY profile_id");

        foreach ($data as $row)
        {
            DB::statement("UPDATE profiles SET rank = " . $row->image . " * 100000 + " . $row->email . " * 50000 + " . $row->viewed_new . " WHERE id = " . $row->id);
        }
    }

    public static function MailSender()
    {
//        $data = DB ::select("SELECT
//                                views.profile_id, count(views.id) as view_count,
//                                mails.created_at
//                            FROM
//                                views
//                            LEFT JOIN
//                                mails ON (views.profile_id = mails.profile_id)
//                            WHERE
//                                DATE_ADD(mails.created_at, INTERVAL 7 DAY) < now()
//                                OR mails.created_at IS NULL
//                            GROUP BY profile_id
//                            HAVING count(views.id) > 5");
        $data = DB ::select("SELECT
                                views.profile_id, count(views.id) as view_count,
                                mails.created_at,
                                profiles.getsEmail
                            FROM
                                views
                            LEFT JOIN
                                mails ON (views.profile_id = mails.profile_id)
                            LEFT JOIN
                                profiles on (profiles.id = views.profile_id)
                            WHERE
                                (DATE_ADD(mails.created_at, INTERVAL 7 DAY) < now()
                                OR mails.created_at IS NULL)
                                AND profiles.getsEmail = true
                            GROUP BY profile_id
                            HAVING count(views.id) > 5");
        //dd($data);
        foreach ($data as $row)
        {
            $profile = Profile::find($row->profile_id);
            if (isset($profile->user->email))
            {
                $email = $profile->user->email;
            }
            else
            {
                $email = $profile->email;
            }

            $token = str_random(50);
            Profile::where('id', $row->profile_id)
                        ->update(['email_token' => $token]);

            Mail::queue('auth.emails.mailSender', [
                'profile_id' => $row->profile_id,
                'token' => $token
            ], function ($message) use ($email)
            {
                $message->from(env('MAIL_USERNAME'));
                $message->to(env('MAIL_RECIPIENT'))->subject('Subscribe');
            });
        }
    }
}
