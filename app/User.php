<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $fillable = ['firstname', 'lastname', 'email', 'password'];
    protected $hidden = ['password', 'remember_token'];

    public function posts()
    {
        return $this->hasMany('App\Posts','user_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments','user_id');
    }

    public function canPost()
    {
        $role = $this->role;
        return $role === 'agent' || $role === 'admin';
    }

    public function isAdmin()
    {
        $role = $this->role;
        return $role === 'admin';
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }
}
