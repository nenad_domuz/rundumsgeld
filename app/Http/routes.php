<?php

/*****************************************************************************************************************************
*
* Simple page (view only)
*
******************************************************************************************************************************/

Route::get('/', function () { return View::make('pages.home'); });
Route::get('versicherungsberatung', function() { return View::make('pages.insuranceConsulting'); });
Route::get('finanzplanung', function() { return View::make('pages.financialPlanning'); });
Route::get('kreditberatung', function() { return View::make('pages.creditConsulting'); });
Route::get('ueber-uns', function() { return View::make('pages.about'); });
Route::get('impressum', function() { return View::make('pages.imprint'); });
Route::get('distance', 'DistanceController@GetDistance');
Route::post('image', 'ImageController@GetImage');
Route::get('imageInitial', 'ImageInitialController@RetriveImage');
Route::get('register/notAllowed', 'Auth\AuthController@index');
Route::get('/kontakt', 'ContactController@index');
Route::post('/kontakt', 'ContactController@send');
Route::post('/agent', 'ContactController@contactAgent');
Route::get('/unsubscribe/{token}/{id}', 'UnsubscribeController@unsubscribe');

/*****************************************************************************************************************************
*
* Routes for business case
*
******************************************************************************************************************************/

Route::get('sitemap.xml', 'SitemapController@index');
Route::get('/berater/{company}/{lastname}/{firstname}/{id}', 'ProfileController@showSingleProfile');
Route::get('/stadt/{city}', 'ProfileController@SearchByCity');
Route::get('/name/{name}', 'ProfileController@SearchByName');
Route::get('/distanz/{distance}', 'ProfileController@SearchByDistance');
Route::get('/unternehmen/{company}', 'ProfileController@SearchByCompany');
Route::get('/service/{service}/{id}', 'ProfileController@SearchByService');
Route::get('/home', 'HomeController@index');

Route::get('/stadt/', 'ProfileController@HomeSearch');
Route::get('/search/json', 'ProfileController@SearchAsJson');

/*****************************************************************************************************************************
 *
 * Auth routes
 *
 ******************************************************************************************************************************/
//Route::auth();
Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);
Route::post('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

// Registration Routes...
Route::get('register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@showRegistrationForm']);
Route::post('register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@register']);

// Password Reset Routes...
Route::get('password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
Route::post('password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
Route::post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);

/*****************************************************************************************************************************
*
* Routes for Profile
*
******************************************************************************************************************************/

Route::group(['middleware' => ['auth'], 'prefix' => 'user'], function()
{
    Route::get('/','UserController@index');
    Route::get('/picture','UserController@picture');
    Route::post('/update','UserController@update');
});


/*****************************************************************************************************************************
*
* Routes for Blog
*
******************************************************************************************************************************/

Route::get('/blog','BlogPostController@index');
Route::get('/blog/home',['as' => 'home', 'uses' => 'BlogPostController@index']);

Route::controllers([
 'auth' => 'Auth\AuthController',
 'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => ['auth']], function()
{
    Route::get('/blog/new-post','BlogPostController@create');
    Route::post('/blog/new-post','BlogPostController@store');
    Route::get('/blog/edit/{slug}','BlogPostController@edit');
    Route::post('/blog/update','BlogPostController@update');
    Route::get('/blog/delete/{id}','BlogPostController@destroy');
    Route::get('/blog/my-all-posts','BlogController@userPostsAll');
    Route::get('/blog/my-drafts','BlogController@userPostsDraft');
    Route::post('/blog/comment/add','BlogCommentController@store');
    Route::post('/blog/comment/delete/{id}','BlogCommentController@destroy');

    Route::get('/admin','UserController@showAdmin');
    Route::get('/admin/posts/all', 'UserController@showAllPosts');
    Route::get('/admin/posts/published', 'UserController@showPublishedPosts');
    Route::get('/admin/posts/waiting', 'UserController@showWaitingPosts');

    Route::post('/admin/approve/{id}', 'UserController@approvePost');
    Route::post('/admin/softDelete/{id}', 'UserController@softDeletePost');
    Route::post('/admin/restore/{id}', 'UserController@restorePost');
    Route::post('/admin/force/{id}', 'UserController@forceDeletePost');
});

Route::get('blog/user/{id}/posts','BlogController@userPosts')->where('id', '[0-9]+');
Route::get('/blog/{slug}',['as' => 'post', 'uses' => 'BlogPostController@show'])->where('slug', '[A-Za-z0-9-_]+');

/*****************************************************************************************************************************
*
* Routes for REST API
*
******************************************************************************************************************************/

Route::group(array('prefix' => 'api/v1'), function()
{
    Route::resource('profiles', 'ApiProfileController');
    Route::resource('users', 'ApiBlogController');
});

/*****************************************************************************************************************************
 *
 * Routes for Registration
 *
 ******************************************************************************************************************************/

Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');
