<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view('pages.contact');
    }

    public function send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'contactEmail' => 'required|email',
            'contactMessage' => 'required|string'
        ]);

        if($validator->fails())
        {
            return redirect('/kontakt')->withErrors($validator->errors());
        }

        Mail::send('auth.emails.contactForm', [
            'name' => $request->input('contactName'),
            'email' => $request->input('contactEmail'),
            'website' => $request->input('contactWebsite'),
            'question' => $request->input('contactMessage'),
            'pathToImage' => public_path().'/images/logo-header.png'
        ], function ($m) {
            $m->from(env('MAIL_RECIPIENT'));
            $m->to(env('MAIL_RECIPIENT'))->subject(trans('controller.contact.subject'));
        });

        session()->flash('status', trans('controller.contact.thankYou'));
        return redirect('/kontakt');
    }

    public function contactAgent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'senderName' => 'required|string',
            'senderEmail' => 'required|email',
            'senderMessage' => 'required|string',
        ]);

        if($validator->fails())
        {
//            return redirect()->back()->withErrors($validator->errors());
            return response()->json($validator->errors(), 400);
        }

        Mail::send('auth.emails.contactFormAgent', [
            'agentFirstname' => $request->input('agentFirstname'),
            'agentLastname' => $request->input('agentLastname'),
            'agentId' => $request->input('agentId'),
            'agentEmail' => $request->input('agentEmail'),
            'senderName' => $request->input('senderName'),
            'senderEmail' => $request->input('senderEmail'),
            'senderMessage' => $request->input('senderMessage'),
            'pathToImage' => public_path().'/images/logo-header.png'
        ], function ($m) use ($request) {
            $m->from($request->input('senderEmail'));
            $m->subject('Rund ums Geld');
            $m->to(env('MAIL_RECIPIENT'))->subject(trans('controller.contact.contacted'));
        });
    }
}
