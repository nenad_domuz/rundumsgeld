<?php

namespace App\Http\Controllers;

use App\Helpers\DataBaseHelper;
use App\Helpers\StringHelper;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use DB;

class DistanceController extends Controller
{
    public static function GetDistance()
    {
//        $lat =Input::get('lat');
//        $lng =Input::get('lng');
        $id = Input::get('id');
        $cookieLat = $_COOKIE['lat'];
        $cookieLng = $_COOKIE['lng'];
        //StringHelper::echoObjectAsStringAndDie($cookieLat);
        $distance = DataBaseHelper::GetBranchesDistance($id, $cookieLat, $cookieLng);

        return isset($distance[0]) ? round($distance[0]->distance) . " Km" : $distance = "N/A";

    }
}
