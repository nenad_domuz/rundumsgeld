<?php

namespace App\Http\Controllers;

use App\Helpers\Distance;
use App\Helpers\ResultsHelper;
use App\Http\Requests;
use App\Profile;
use App\Helpers\StringHelper;
use App\Helpers\DataBaseHelper;
use App\Helpers\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
class ProfileController extends Controller
{
    public function HomeSearch()
    {
        $city = Input::get('stadt');
        $query = DataBaseHelper::GetBasicProfileSearchQuery()->where('branches.city', 'LIKE', '%'.$city  .'%');

        $data['Profiles'] = $query->paginate(6)->setPath('stadt/'.$city);

        return empty($data['Profiles']) || $city == '' || !$query->count() ? view('pages.searchNoResults') :  view('pages.search', $data);
    }

    public function SearchAsJson()
    {
        $results = DataBaseHelper::GetCitiesOnly()->take(5)->where('city', 'LIKE', '%'.request('query').'%')->get();

        return response()->json([
            'suggestions' => $results
        ]);
    }

    public function showSingleProfile($company, $lastname, $firstname, $id)
    {
        $data['profile'] = Profile::find($id);
        $data['branches'] = Profile::find($id)->branches;
        $data['company'] = DataBaseHelper::GetBasicProfileSearchQuery()->where('profiles.id', '=', $id)->groupBy('profiles.id')->get();

        if (\App::environment('local') and (
                $id ==  StringHelper::transformUrl($data['profile']->id) or
                $lastname == StringHelper::transformUrl($data['profile']->lastname) or
                $firstname == StringHelper::transformUrl($data['profile']->firstname) or
                $company == StringHelper::transformUrl($data['company'][0]->CompanyName)))
        {
            return view('pages.profile', $data);

        } else if (!\App::environment('local') and (
                $id ==  StringHelper::transformUrl($data['profile']->id) and
                $lastname == StringHelper::transformUrl($data['profile']->lastname) and
                $firstname == StringHelper::transformUrl($data['profile']->firstname) and
                $company == StringHelper::transformUrl($data['company'][0]->CompanyName)))
        {
            return view('pages.profile', $data);
        } else
        {
            abort(404);
        }
    }

    public function SearchByCity($city)
    {
        $query = DataBaseHelper::GetBasicProfileSearchQuery()->where('branches.city', $city);
        $data['Profiles'] = $query->paginate(6);

        return empty($data['Profiles']) ? view('pages.searchNoResults') : view('pages.search', $data);
    }

    public function SearchByName($name)
    {
        $query = DataBaseHelper::GetBasicProfileSearchQuery()->where('profiles.lastname', $name);
        $profiles = $query->get();

        return view('pages.search', $profiles);
    }

    public function SearchByCompany($company)
    {
        $companyName = StringHelper::companyNameFromURI($company);
        $query = DataBaseHelper::GetBasicProfileSearchQuery()->where('companies.name', 'like', "%$companyName%")->groupBy('profiles.id');
        $data['Profiles'] = $query->paginate(6);

        return empty($data['Profiles']) ? view('pages.searchNoResults') :  view('pages.search', $data);

    }

    public function SearchByService($service, $id)
    {
        $query = DataBaseHelper::GetBasicProfileSearchQueryProducts()->where('products.id', '=', $id)->groupBy('profiles.id');
        $data['Profiles'] = $query->paginate(6);

        if (\App::environment('local') and (
                $service == StringHelper::transformUrl($data['Profiles'][0]->ProductName) or
                $id == StringHelper::transformUrl($data['Profiles'][0]->ProductId)))
        {
            return view('pages.search', $data);
        } else if (!\App::environment('local') and (
                $service == StringHelper::transformUrl($data['Profiles'][0]->ProductName) and
                $id == StringHelper::transformUrl($data['Profiles'][0]->ProductId)))
        {
            return view('pages.search', $data);
        }
        else
        {
            abort(404);
        }
    }
    public function SearchByDistance(Request $request, $distance)
    {
        $query = DataBaseHelper::GetSearchDistance($distance);
//        StringHelper::echoObjectAsStringAndDie($query);
        if ($query)
        {
            $page = Input::get('page', 1);
            $perPage = 6;
            $offset = ($page * $perPage) - $perPage;

            $data['Profiles'] = new LengthAwarePaginator(array_slice($query, $offset, $perPage, true), count($query), $perPage, $page, ['path' => $request->url(), 'query' => $request->query()]);
            return empty($data['Profiles']) ? view('pages.searchNoResults') : view('pages.search', $data);
        }
        else
        {
            return view('pages.searchNoResults');
        }
    }
}