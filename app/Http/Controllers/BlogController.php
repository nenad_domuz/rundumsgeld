<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Posts;

class BlogController extends Controller
{
    public function userPosts($id)
    {
        $posts = Posts::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(5);
        //$title = User::find($id)->firstname . ' ' . User::find($id)->lastname;
        $action = explode('@',\Route::current()->getActionName());

        return view('blog.home')->withPosts($posts)->withAction($action);
    }

    public function userPostsAll(Request $request)
    {
        $user = $request->user();
        $posts = Posts::where('user_id', $user->id)->orderBy('created_at', 'desc')->paginate(5);
        $title = $user->firstname;
        return view('blog.home')->withPosts($posts)->withTitle($title);
    }

    public function userPostsDraft(Request $request)
    {
        $user = $request->user();
        $posts = Posts::where('user_id', $user->id)->where('active', 0)->orderBy('created_at', 'desc')->paginate(5);
        $title = $user->firstname .' '.$user->lastname;

        return view('blog.home')->withPosts($posts)->withTitle($title);
    }

    public function profile(Request $request, $id)
    {
        $data['user'] = User::find($id);

        if ($request->user() && $data['user']->id == $request->user()->id)
        {
            $data['comments_count']     = $data['user']->comments->count();
            $data['posts_count']        = $data['user']->posts->count();
            $data['posts_active_count'] = $data['user']->posts->where('active', 1)->count();
            $data['posts_draft_count']  = $data['posts_count'] - $data['posts_active_count'];
            $data['latest_posts']       = $data['user']->posts->take(5);
            $data['latest_comments']    = $data['user']->comments->take(5);

            return view('admin.profile', $data);
        }
        else
        {
            return redirect('/blog');
        }

    }
}