<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ImageController extends Controller
{
    public static function GetImage()
    {
        $id = Auth::user()->id;
        $input = Input::all();

        DB::table('profiles')
            ->where('user_id', $id)
            ->update([
            'image_blob' =>  $input[0]
        ]);

        DB::table('profiles')
            ->where('user_id', $id)
            ->update([
                'image' =>  ''
            ]);
    }
}
