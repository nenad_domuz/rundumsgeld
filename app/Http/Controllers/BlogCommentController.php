<?php

namespace App\Http\Controllers;

use App\Posts;
use App\Comments;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
// do we need this if class extended the controller already?
use App\Http\Controllers\Controller;

class BlogCommentController extends Controller
{
    public function store(Request $request) 
    {
        $input['user_id'] = $request->user()->id;
        $input['post_id'] = $request->input('post_id');
        $input['body'] = $request->input('body');

        $slug = $request->input('slug');
        
        Comments::create( $input );
        
        return redirect('/blog/'.$slug)->with('message', trans('controller.blogComment.published'));
    }
}