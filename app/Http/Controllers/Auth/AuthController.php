<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\StringHelper;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\ActivationService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    protected $activationService;

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new authentication controller instance.
     *
     */

    public function index()
    {
        return view('errors.notAllowed')->withErrors(trans('gui.register.notAllowed'));
    }


    public function __construct(ActivationService $activationService)
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->activationService = $activationService;
    }

    public function activateUser(Request $request, $token)
    {
        if ($user = $this->activationService->activateUser($token))
        {
            auth()->login($user);
            $request->session()->flash('status', trans('controller.auth.activated'));
            return view('pages.userActivated');
        }

        abort(404);
    }

//    public function authenticated($user)
//    {
//        if (!$user->activated)
//        {
//            //$this->activationService->sendActivationMail($user);
//            auth()->logout();
//            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
//        }
//        return redirect()->intended($this->redirectPath());
//
//    }

    public function register(Request $request)
    {
        $validatorBasic =  Validator::make($request->all(), [
            'firstname' => 'required|min:2|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|unique:users,email|min:8|max:254',
            'finma_registration_number' => 'digits_between:1,5',
            'password' => 'required|min:6|confirmed',
        ]);

        if($validatorBasic->fails())
        {
            return redirect('/register')->withErrors($validatorBasic)->withInput($request->all());
        }

        if ($this->isValidEmailAddress($request->all()))
        {
            $this->doRegistration($request);
            return redirect('/register')->with('status', trans('controller.auth.mailSent'));
        }

        if ($this->isValidFinmaNumber($request->all()))
        {
            $this->doRegistration($request);
            return redirect('/register')->with('status', trans('controller.auth.mailSent'));
        }

        Mail::send('auth.emails.notAllowed', [
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
        ], function ($message) {
            $message->to('nenad.domuz@gmail.com', 'ASVM Info')->subject('Registration Fail Log!');
        });

        return redirect('/register/notAllowed');
    }


    private function isValidEmailAddress(array $data)
    {
        $exists = DB::table('profiles')
            ->where([
                ['email', $data['email']],
            ])->first();

        return (empty($exists)) ? false : true;
    }


    protected function isValidFinmaNumber(array $data)
    {
        $exists = DB::table('profiles')
            ->where([
                ['finma_registration_number', $data['finma_registration_number']],
                ['firstname', $data['firstname']],
                ['lastname', $data['lastname']],
            ])->first();

        return (empty($exists)) ? false : true;
    }

    private function doRegistration(Request $request)
    {
        $user= $this->create($request->all());

        //Log
        $userId = $user->id;
        $this->createLog($userId);

        Mail::send('auth.emails.registrationLog', [
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'email' => $user->email,
        ], function ($message) use ($user) {
            $message->to('info@rundumsgeld.ch', 'ASVM Info')->subject('Registration Log!');
        });

        $this->activationService->sendActivationMail($user);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

    protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    protected function createLog($userId)
    {
        return DB::table('log')->insert([
            'user_id' => $userId,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
