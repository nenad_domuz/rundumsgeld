<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Profile;
use GuzzleHttp\Client;
use Mockery\Exception;
use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;

class ApiProfileController extends Controller
{
    protected $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function index()
    {
        $profiles = [];
        
        try 
        {
            $statusCode = 200;
            $profiles = Profile::all(array('id', 'firstname', 'lastname'));
        }
        catch (Exception $e)
        {
            $statusCode = 400;
        }
        finally
        {
            return \Response::json($profiles, $statusCode);
        }
	}

    public function show()
    {
        $resource = [];
        $jsonData = [];
        $client = new Client;

        try
        {
//            $resource = $client->post('http://jsonplaceholder.typicode.com/users', [
//                'json' => [
//                    'id' => 'something from the request'
//                ]
//            ]);

            // this is getting some data
            $resource = $client->request('GET', 'http://jsonplaceholder.typicode.com/users',[
                'timeout' => 0
            ]);

            $jsonData = json_decode($resource->getBody());

            // write to database ?

            $this->mailer->send('auth.emails.calwizzUser', ['name' => $jsonData[0]->name, 'user' => $jsonData[0]->username, 'email' => $jsonData[0]->email], function(Message $m)
            {
                $m->from('no-reply@rundumsgeld.ch', 'Your Activation');
                $m->to('info@rundumsgeld.ch')->subject('Willkommen Calwizz user');
            });
        }
        catch (Exception $e)
        {
            return $resource->getStatusCode();
        }
        finally
        {
            return $jsonData;
        }
    }
    
}
