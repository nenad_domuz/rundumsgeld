<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class imageInitialController extends Controller
{
    public static function RetriveImage()
    {
        $id = Auth::user()->id;

       $image = DB::table('profiles')->select('image', 'image_blob')->where('user_id', $id)->get();
        if ($image[0]->image != '')
        {
            $path = $image[0]->image;
//            $pathExplode = explode('/', $path);
//            $path = 'images/' . array_pop($pathExplode);
            return 'images/'.$path;
        }
        if ($image[0]->image_blob != '') {
            $pathBlob = $image[0]->image_blob;
            return $pathBlob;
        }
        else
        {
            return '';
        }

    }
}
