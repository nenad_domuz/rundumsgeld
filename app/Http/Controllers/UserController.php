<?php

namespace App\Http\Controllers;

use App\Comments;
use App\Helpers\DataBaseHelper;
use App\Helpers\StringHelper;
use App\Posts;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mail;

class UserController extends Controller
{
    public function index()
    {
        $userdata = Auth::user();
        $profiledata = DataBaseHelper::GetBasicProfileSearchQuery()->where('user_id', $userdata->id)->get();

        if(empty($profiledata))
        {
            return $this->showAdmin();
        }
        else
        {
            return view('pages.user')->withUserdata($userdata)->withProfiledata($profiledata);
        }
    }

    public function showAdmin()
    {
        $data['postsCount'] = Posts::all()->count();
        $data['postsPublished'] = Posts::where('active', 1)->count();
        $data['postsForApproval'] = Posts::where('active', 0)->count();
        $data['commentsCount'] = Comments::all()->count();

        return view('admin.profile', $data);
    }

    public function showAllPosts ()
    {
        $data['posts'] = Posts::withTrashed()->get();

        return view('admin.allPosts', $data);
    }

    public function showPublishedPosts ()
    {
        $data['publishedPosts'] = Posts::where('active', 1)->get();

        return view('admin.publishedPosts', $data);
    }

    public function showWaitingPosts ()
    {
        $data['waitingPosts'] = Posts::where('active', 0)->get();

        return view('admin.waitingPosts', $data);
    }

    public function approvePost ($id, Request $request)
    {
        Posts::where('id', $id)
            ->update(['active' => 1]);

        $request->session()->flash('status', 'Approved');
        return redirect('/admin/posts/waiting');
    }

    public function softDeletePost($id, Request $request)
    {
        Posts::destroy($id);

        $request->session()->flash('status', 'Soft Deleted');
        return redirect('admin/posts/all');
    }

    public function restorePost($id, Request $request)
    {
        Posts::withTrashed()
            ->where('id', $id)
            ->restore();

        $request->session()->flash('status', 'Restored');
        return redirect('admin/posts/all');
    }

    public function forceDeletePost($id, Request $request)
    {
        Posts::where('id', $id)
            ->forceDelete();

        $request->session()->flash('status', 'Deleted');
        return redirect('admin/posts/all');
    }

    public function picture()
    {
        return view('pages.picture');
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'phone' => 'required|digits_between:9,12',
            'aboutMe' => 'required',
            'fax' => 'digits_between:9,12',
            'mobile' => 'digits_between:9,12',
            'hobbies' => 'string',
            'training' => 'string',
            'skills' => 'string',
            'education' => 'string',
            'language' => 'string',
            'dob' => 'date_format:Y-m-d'
        ]);

        if ($validator->fails())
        {
            return redirect('/user')->withErrors($validator);
        }

        DB::table('profiles')->where('user_id', $user->id)->update([
            'phone' => $request->input('phone'),
            'fax' => $request->input('fax'),
            'mobile' => $request->input('mobile'),
            'about_me' => $request->input('aboutMe'),
            'hobbies' => $request->input('hobbies'),
            'training' => $request->input('training'),
            'skills' => $request->input('skills'),
            'education' => $request->input('education'),
            'language' => $request->input('language'),
            'date_of_birth' => $request->input('dob'),
            'longitude' => $request->input('lng'),
            'latitude' => $request->input('lat')
        ]);

        $profileId = auth()->user()->profile->id;
        $companyName = auth()->user()->profile->branches[0]->name;
        /*
         * part of url is hardcoded,
         * if you use $request->url() to grab the url,
         * it grabs the route url for this method e.g https://www.rundumsgeld.ch/update
         * */

        $link = StringHelper::transformUrl('/berater/'.$companyName.'/'.$user->lastname.'/'.$user->firstname.'/'.$profileId);

        Mail::send('auth.emails.profileUpdate', [
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'link' => $link,
            'email' => $user->email,
        ], function ($message) use ($user)
        {
            $message->to(env('MAIL_RECIPIENT_ASVM'), 'ASVM Info')->subject( $user->firstname . ' ' . $user->lastname . ' ' . trans('emails.profile.update.email.subject'));
        });

        $request->session()->flash('status', trans('errors.user.profile.button.update'));
        return redirect('/user');
    }
}
