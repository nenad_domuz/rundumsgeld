<?php

namespace App\Http\Controllers;

use App\Posts;
use App\Profile;
use App\Helpers\StringHelper;

ini_set('memory_limit','1024M');


class SitemapController extends Controller
{
    public function index() {
        $url = url('/');
        $blogUrl = url('/blog');

        $entries = [];

        $posts = Posts::select('slug')->get();
        foreach ($posts as $post)
        {
            $singlePost = [
                'loc' => $blogUrl . StringHelper::transformUrl('/'.$post->slug),
                'changefreq' => 'weekly',
                'lastmod' => '2016-06-26',
                'priority' => '1.0'
            ];

            array_push($entries, $singlePost);
        }

        $profiles = Profile::select('id','firstname','lastname')->with(array('branches' => function($query) {
            $query->select('id', 'company_id', 'name');
        }))->get();

        foreach ($profiles as $profile)
        {
            $singleProfile = [
                'loc' => $url .StringHelper::createProfilePageUrl($profile->branches[0]->company->name, $profile->lastname, $profile->firstname, $profile->id),
                'changefreq' => 'weekly',
                'lastmod' => '2016-06-26',
                'priority' => '1.0'
            ];

            array_push($entries, $singleProfile);
        }

        return response()->view('sitemap', array('entries' => $entries, 'head' => '<?xml version="1.0" encoding="UTF-8"?>'))->header('Content-Type', 'text/xml');
    }
}


