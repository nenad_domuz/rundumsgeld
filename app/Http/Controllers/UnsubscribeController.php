<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

use App\Http\Requests;

class UnsubscribeController extends Controller
{
    public function unsubscribe($token, $id, Request $request)
    {
        $tokenUrl = $request->segment(2);
        $tokenDB = Profile::find($id);

        if ($tokenUrl == $tokenDB->email_token)
        {
            $profile = Profile::find($id);

            $profile->getsEmail = 0;

            $profile->save();
        }

        return redirect('/');
    }
}
