<?php

namespace App\Http\Controllers;

use App\Helpers\DataBaseHelper;
use App\Helpers\StringHelper;
use Illuminate\Http\Request;
use App\Posts;
use App\Http\Requests;
use App\Http\Requests\PostFormRequest;
use Illuminate\Support\Facades\Mail;

class BlogPostController extends Controller
{
    public function index()
    {
        $posts = Posts::where('active', 1)->orderBy('created_at', 'desc')->paginate(5);
        $title = trans('controller.blogPost.title');

        return view('blog.home')->withPosts($posts)->withTitle($title);
    }

    public function create(Request $request)
    {
        if ($request->user()->canPost())
        {
            return view('posts.create');
        }
        else
        {
            $request->session()->flash('danger', trans('controller.blogPost.permission'));
            return redirect('/blog/');
        }
    }

    public function store(PostFormRequest $request)
    {
        $post = new Posts();
        $post->title = $request->get('title');
        $post->body = $request->get('body');
        $post->slug = StringHelper::transformUrl($post->title);
        $post->user_id = $request->user()->id;

        if (Posts::where('title', '=', $post->title)->exists())
        {
            $request->session()->flash('warning', trans('controller.blogPost.titleExists'));
            return redirect('/blog/new-post')->withInput();
        }
        else
        {
            if(auth()->user()->isAdmin())
            {
                $post->active = 1;
                $request->session()->flash('status', trans('controller.blogPost.saved'));
            }
            else
            {
                $post->active = 0;
                $request->session()->flash('status', trans('controller.blogPost.published'));
            }

            $post->save();

            Mail::send('auth.emails.newBlogPost', [
                'title' => $request->input('title'),
                'userId' => $request->user()->id,
            ], function ($m) {
                $m->from('info@rundumsgeld.ch');
                $m->subject(trans('emails.blog.newPost.title'));
                $m->to(env('MAIL_RECIPIENT'));
            });

            return redirect('/blog');
        }
    }

    public function show($slug)
    {
        $post = Posts::where('slug', $slug)->first();

        if (!$post)
        {
            return redirect('/blog')->withErrors(trans('controller.blogPost.requestNotFound'));
        }

        $comments = DataBaseHelper::GetCommentsWithUsers($slug);

        return view('posts.show')->withPost($post)->withComments($comments);
    }

    public function edit(Request $request, $slug)
    {
        $post = Posts::withTrashed()
            ->where('slug', $slug)
            ->first();

        if ($post && ( $request->user()->id == $post->user_id) || $request->user()->isAdmin())
        {
            return view('posts.edit')->with('post', $post);
        }
        $request->session()->flash('danger', trans('controller.blogPost.permission'));
        return redirect('/blog');
    }

    public function update(Request $request)
    {
        $post_id = $request->input('post_id');
        $post = Posts::find($post_id);
        if($post && ($post->author_id == $request->user()->id || $request->user()->isAdmin() || $request->user()->canPost()))
        {
            $title = $request->input('title');
            $slug = str_slug($title);
            $duplicate = Posts::where('slug',$slug)->first();
            if($duplicate)
            {
                if($duplicate->id != $post_id)
                {
                    return redirect('/blog/edit/'.$post->slug)->withErrors(trans('controller.blogPost.titleExists'))->withInput();
                }
                else
                {
                    $post->slug = $slug;
                }
            }
            $post->title = $title;
            $post->body = $request->input('body');
            if($request->has('save'))
            {
                $post->active = 0;
                $request->session()->flash('status', trans('controller.blogPost.saved'));
                $landing = '/blog';
            }
            else {
                $post->active = 1;
                $request->session()->flash('status', trans('controller.blogPost.saved'));
                $landing = 'blog/edit/'.$post->slug;
            }
            $post->save();
            
            return redirect($landing);
        }
        else
        {
            $request->session()->flash('danger', trans('controller.blogPost.permission'));
            return redirect('/blog');
        }
    }

    public function destroy(Request $request, $id)
    {
        $post = Posts::find($id);
        
        if ($post && ($post->user_id === $request->user()->id || $request->user()->isAdmin()))
        {
            $request->session()->flash('status', trans('controller.blogPost.deleted'));
            $post->delete();
        }
        else
        {
            $request->session()->flash('danger', trans('controller.blogPost.permission'));
        }
        return redirect('/blog');
    }
}
