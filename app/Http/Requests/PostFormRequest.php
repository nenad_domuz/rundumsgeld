<?php

namespace App\Http\Requests;

class PostFormRequest extends Request
{
    public function authorize()
    {
        if($this->user()->canPost())
        {
            return true;
        }
        return false;
    }

    public function rules()
    {
        return [
            'title' => 'required|unique:posts|max:255|Regex:/^[ -\.,\&áÁàÀăĂâÂåÅãÃąĄāĀäÄæÆḃḂćĆĉĈčČċĊçÇďĎḋḊđĐðÐéÉèÈĕĔêÊěĚëËėĖęĘēĒḟḞƒƑğĞĝĜġĠģĢĥĤħĦíÍìÌîÎïÏĩĨįĮīĪĵĴķĶĺĹľĽļĻłŁṁṀńŃňŇñÑņŅóÓòÒôÔőŐõÕøØōŌơƠöÖṗṖŕŔřŘŗŖśŚŝŜšŠṡṠşŞșȘßťŤṫṪţŢțȚŧŦúÚùÙŭŬûÛůŮűŰũŨųŲūŪưƯüÜẃẂẁẀŵŴẅẄýÝỳỲŷŶÿŸźŹžŽżŻþÞµаАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯ0-9A-Za-z]+$/',
            'body' => 'required'
        ];
    }
}
