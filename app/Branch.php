<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';

    public function profiles()
    {
        return $this->belongsToMany('App\Profile', 'profiles_branches', 'profile_id', 'branch_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
