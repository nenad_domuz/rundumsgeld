<?php

namespace App\Console\Commands;

use App\Helpers\DataBaseHelper;
use Illuminate\Console\Command;

class UpdateRank extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rank:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the ranks field in the Profiles table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DataBaseHelper::UpdateProfileRank();
        $this->info('Triggered succesfully');
    }
}
