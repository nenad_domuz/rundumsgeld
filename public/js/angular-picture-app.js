angular.module('app', ['ngImgCrop'])
    .controller('Ctrl', function($scope, $http, $window) {
        $scope.myImage = '';
        $scope.myCroppedImage = '';
        //$scope.buttonSave = true;

        $http.get('/imageInitial').success(function (result){
                $scope.myImage = result;
            }
        );
        $scope.sendImageData = function () {
            var serviceBase = '/image';
            return $http.post(serviceBase, JSON.stringify($scope.myCroppedImage)).success(function (result){
                location.reload();
            });

        }

        var handleFileSelect=function(evt) {
            var file=evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.myImage=evt.target.result;
                });
            };
            //if($scope.myCroppedImage != '')
            //{
            //    $scope.buttonSave = true;
            //}

            reader.readAsDataURL(file);
            console.log(file);
            //console.log($scope.myCroppedImage);
        };

        angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);
    });
