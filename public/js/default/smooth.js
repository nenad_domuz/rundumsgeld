$(document).ready(function(){
	$(window).scroll(function(){
		if ($(this).scrollTop() > 300) {
			$('#page-back').fadeIn('slow');
		} else {
			$('#page-back').fadeOut('slow');
		}
	});
	$('#page-back').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});

});